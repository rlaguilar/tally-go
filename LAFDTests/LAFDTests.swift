//
//  LAFDTests.swift
//  LAFDTests
//
//  Created by Anthony Picciano on 11/2/16.
//  Copyright © 2016 TALLYGO. All rights reserved.
//

import XCTest
import TallyGoKit
@testable import LAFD

class LAFDTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testEquipment() {
        let list = Equipment.get()
        
        for eq in list {
            debugPrint("\(eq.name!) located at station \(eq.stationId)")
            
            if eq.latitude == 0 || eq.longitude == 0 {
                XCTFail("Equipment is missing location.")
            }
        }
        
        XCTAssert(list.count > 5, "Not enough equipment.")
    }
    
    func testEquipmentAsyncronous() {
        let expectation = self.expectation(description: "testEquipmentAsyncronous")
        var list: [Equipment]!
        
        Equipment.get { (result) in
            list = result
            
            for eq in list! {
                debugPrint("\(eq.name!) located at station \(eq.stationId)")
                
                if eq.latitude == 0 || eq.longitude == 0 {
                    XCTFail("Equipment is missing location.")
                }
            }
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("Expectation failed with error: \(error)");
            }
            
            XCTAssert(list.count > 5, "Not enough equipment.")
        }
    }
    
    func testEquiementById() {
        let eq = Equipment.get(objectId: "DEDE0330-F6D3-815D-FF60-B05E272CF200")
        
        debugPrint(eq?.fullAddress as Any)
        debugPrint(eq?.coordinate as Any)
        
        XCTAssertNotNil(eq, "Expected equipment is nil.")
    }
    
    func testEquiementByIdAsyncronous() {
        let expectation = self.expectation(description: "testEquipmentAsyncronous")
        var eq: Equipment!
        
        Equipment.get(objectId: "DEDE0330-F6D3-815D-FF60-B05E272CF200") { (result) in
            eq = result
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("Expectation failed with error: \(error)");
            }
            
            XCTAssertNotNil(eq, "Expected equipment is nil.")
        }
    }
    
    func testFacilityAsyncronous() {
        let expectation = self.expectation(description: "testFacilityAsyncronous")
        var list: [Facility]!
        
        Facility.get { (result) in
            list = result
            
            for eq in list! {
                debugPrint("\(eq.name!) : \(eq.facilityNumber)")
                
                if eq.latitude == 0 || eq.longitude == 0 {
                    XCTFail("Facility is missing location.")
                }
            }
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("Expectation failed with error: \(error)");
            }
            
            XCTAssert(list.count > 2, "Not enough facilities.")
        }
    }
    
    func testConfiguration() {
        let expectation = self.expectation(description: "testConfigurationSimulataneusRequests")
        var config: Configuration?
        
        Configuration.get(key: .rootUrl) { result in
            config = result
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("Expectation failed with error: \(error)");
            }
            
            XCTAssertEqual(config?.value, "https://lafd.tallygo.com", "Root URL is incorrect.")
        }
    }
    
    func testAsset() {
        let expectation = self.expectation(description: "testAsset")
        let assetId = "E41"
        
        var response: AssetResponse?
        
        Asset.get(request: AssetRequest(assetId: assetId)) { resp in
            response = resp
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("Expectation failed with error: \(error)");
            }
            
            XCTAssertNotNil(response, "Expected response is nil.")
            XCTAssertNotNil(response?.asset?.coordinate, "Asset coordinate should not be nil.")
            XCTAssertNotNil(response?.asset?.statusDate, "Status date should not be nil.")
            XCTAssertNotNil(response?.asset?.incident?.coordinate, "Incident coordinate should not be nil.")
            XCTAssertNotNil(response?.asset?.incident?.streetAddress, "Incident street address should not be nil.")
        }
    }
    
    func testIncidents() {
        let expectation = self.expectation(description: "testIncidents")
        
        var response: IncidentResponse?
        
        Incident.getAll { resp in
            response = resp
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("Expectation failed with error: \(error)");
            }
            
            XCTAssertNotNil(response, "Expected response is nil.")
            XCTAssertNotNil(response?.featureCollection, "Feature collection is nil.")
            
            let first = response?.featureCollection?.features.first
            let point = first?.geometry as? TGPointShape
            XCTAssertNotNil(first, "First feature is nil.")
            XCTAssertNotNil(point, "Point is nil.")
            XCTAssertNotNil(point?.coordinate, "Point coordinate is nil.")
            XCTAssertNotNil(first?.incident, "First incident is nil.")
            XCTAssertNotNil(first?.incident?.incidentNumber, "First incident number is nil.")
            XCTAssertNotNil(first?.incident?.coordinate, "First incident coordinate is nil.")
            XCTAssertEqual(first?.incident?.coordinate?.latitude, point?.coordinate?.latitude, "Point latitudes do not match.")
            XCTAssertEqual(first?.incident?.coordinate?.longitude, point?.coordinate?.longitude, "Point longitudes do not match.")
        }
    }
    
}
