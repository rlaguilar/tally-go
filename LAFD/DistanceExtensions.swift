//
//  DistanceExtensions.swift
//  LAFD
//
//  Created by Anthony Picciano on 12/21/16.
//  Copyright © 2016 TALLYGO. All rights reserved.
//

import Foundation
import CoreLocation

extension CLLocationCoordinate2D {
    
    func distance(from coordinate: CLLocationCoordinate2D) -> CLLocationDistance {
        let location = CLLocation(latitude: latitude, longitude: longitude)
        return location.distance(from: coordinate)
    }
    
}

extension CLLocation {
    
    func distance(from coordinate: CLLocationCoordinate2D) -> CLLocationDistance {
        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        return distance(from: location)
    }
    
}

extension CLLocationDistance {
    
    var mileString: String {
        let miles = self * 0.000621371
        return String(format: "%.1f mi", miles)
    }
    
}
