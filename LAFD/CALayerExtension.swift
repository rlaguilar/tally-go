//
//  CALayerExtension.swift
//  LAFD
//
//  Created by Anthony Picciano on 11/22/16.
//  Copyright © 2016 TALLYGO. All rights reserved.
//

import Foundation
import UIKit

extension CALayer {
    
    func setIBShadowColor(color: UIColor) {
        shadowColor = color.cgColor
    }
    
    func setIBBorderColor(color: UIColor) {
        borderColor = color.cgColor
    }
    
}
