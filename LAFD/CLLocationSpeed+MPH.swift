//
//  CLLocationSpeed+MPH.swift
//  LAFD
//
//  Created by Anthony Picciano on 11/22/16.
//  Copyright © 2016 TALLYGO. All rights reserved.
//

import Foundation
import CoreLocation

fileprivate let kilometersPerMile = 2.23694

extension CLLocationSpeed {
    
    var mphValue: Int {
        get {
            return Int(self * kilometersPerMile)
        }
    }
    
    init(mph: Int) {
        self = Double(mph) / kilometersPerMile
    }
    
}
