//
//  SizeupStory.swift
//  First Due Mobile
//
//  Created by Reynaldo Aguilar on 11/8/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import SwiftyJSON

struct SizeupStory {
   let story: String
   let url: URL
}

extension SizeupStory {
   init(_ object: Any) throws {
      try self.init(json: JSON(object))
   }
   
   init(json: JSON) throws {
      story = json["story"].stringValue
      
      guard let urlStr = json["url"].string,
            let url = URL(string: urlStr) else {
         throw AppError.invalidJson(key: "url")
      }
      
      self.url = url
   }
}
