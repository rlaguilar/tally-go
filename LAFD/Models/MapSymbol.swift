//
//  Symbol.swift
//  First Due Mobile
//
//  Created by Reynaldo Aguilar on 11/1/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import SwiftyJSON

struct MapSymbol {
   let code: String
   let url: URL
   
   init(json: JSON) throws {
      guard let code = json["code"].string else {
         throw AppError.invalidJson(key: "code")
      }
      
      guard let urlStr = json["url"].string,
         let curedSrt = urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed),
         let url = URL(string: curedSrt) else {
         throw AppError.invalidJson(key: "url")
      }
      
      self.code = code
      self.url = url
   }
}
