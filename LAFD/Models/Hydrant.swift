//
//  Hydrant.swift
//
//  Created by Reynaldo Aguilar on 9/8/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public struct Hydrant {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let latitude = "latitude"
    static let id = "id"
    static let longitude = "longitude"
    static let numOfOutlets = "num_of_outlets"
    static let code = "icon_code"
  }

  // MARK: Properties
  public var latitude: Double
  public var id: Int
  public var longitude: Double
  public var numOfOutlets: Int?
   public var code: String
  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public init(json: JSON) {
    latitude = json[SerializationKeys.latitude].doubleValue
    id = json[SerializationKeys.id].intValue
    longitude = json[SerializationKeys.longitude].doubleValue
    numOfOutlets = json[SerializationKeys.numOfOutlets].int
    code = json[SerializationKeys.code].stringValue
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.latitude] = latitude
    dictionary[SerializationKeys.id] = id
    dictionary[SerializationKeys.longitude] = longitude
    dictionary[SerializationKeys.code] = code
    if let value = numOfOutlets { dictionary[SerializationKeys.numOfOutlets] = value }
    return dictionary
  }

}
