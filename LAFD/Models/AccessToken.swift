//
//  AccessToken.swift
//
//  Created by Reynaldo Aguilar on 8/25/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class AccessToken {
   
   // MARK: Declaration for string constants to be used to decode and also serialize.
   private struct SerializationKeys {
      static let timeZone = "time_zone"
      static let name = "name"
      static let area = "area"
      static let authToken = "access_token"
      static let dateFormat = "date_format"
      static let scope = "scope"
      static let tokenType = "token_type"
      static let expiresIn = "expires_in"
      static let expiresTimeInterval = "expires_time_interval"
   }
   
   // MARK: Properties
   public var timeZone: String
   public var name: String
   public var area: String
   public var authToken: String
   public var dateFormat: String
   public var scope: String
   public var tokenType: String
   public var expirationDate: Date
   
   public convenience init(object: Any) {
      self.init(json: JSON(object))
   }
   
   public required init(json: JSON) {
      timeZone = json[SerializationKeys.timeZone].stringValue
      name = json[SerializationKeys.name].stringValue
      area = json[SerializationKeys.area].stringValue
      authToken = json[SerializationKeys.authToken].stringValue
      dateFormat = json[SerializationKeys.dateFormat].stringValue
      scope = json[SerializationKeys.scope].stringValue
      tokenType = json[SerializationKeys.tokenType].stringValue
      
      // HACK: This is a workaround to parse access token from json and
      // also for parsing it when it is stored in the NSUserDefaults
      if let expirationSeconds = json[SerializationKeys.expiresIn].double {
         expirationDate = Date().addingTimeInterval(expirationSeconds)
      }
      else if let expirationTimeInterval = json[SerializationKeys.expiresTimeInterval].double {
         expirationDate = Date(timeIntervalSince1970: expirationTimeInterval)
      }
      else {
         fatalError()
      }
   }
   
   public func dictionaryRepresentation() -> [String: Any] {
      var dictionary: [String: Any] = [:]
      dictionary[SerializationKeys.timeZone] = timeZone
      dictionary[SerializationKeys.name] = name
      dictionary[SerializationKeys.area] = area
      dictionary[SerializationKeys.authToken] = authToken
      dictionary[SerializationKeys.dateFormat] = dateFormat
      dictionary[SerializationKeys.scope] = scope
      dictionary[SerializationKeys.tokenType] = tokenType
      dictionary[SerializationKeys.expiresTimeInterval] = expirationDate.timeIntervalSince1970
      return dictionary
   }
}
