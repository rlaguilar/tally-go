// 
//  Unit.swift
//  First Due Mobile
//
//  Created by Reynaldo Aguilar on 12/14/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Unit {
   let name: String
   let type: String
   let latitude: Double
   let longitude: Double
   let updatedAt: Date
}

extension Unit {
   init(json: JSON) throws {
      name = json["name"].stringValue
      type = json["type"].stringValue
      latitude = json["latitude"].doubleValue
      longitude = json["longitude"].doubleValue
      
      let dateStr = json["updated_at"].stringValue
      
      guard let date = Date(universalStr: dateStr) else {
         throw AppError.invalidJson(key: "updated_at")
      }
      
      updatedAt = date
   }
}
