//
//  Alert.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/31/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import SwiftyJSON
import TallyGoKit
import CoreLocation

struct Alert {
   let id: Int
   let dispatchType: String
   let createdAt: Date
   
   let address: String?
   let addressId: Int?
   let latitude: Double?
   let longitude: Double?
   let url: URL?
   let city: String?
   let stateCode: String?
   let zip: String?
   let dispatchUnits: [String]
//   let story: String?
   
   let fullMessage: String?
   
   var coordinates: CLLocationCoordinate2D? {
      guard let lat = self.latitude, let lng = self.longitude else {
         return nil
      }
      
      return CLLocationCoordinate2D(latitude: lat, longitude: lng)
   }
   
   var tallyGoAddress: TGAddress? {
      guard let address = self.address else { return nil }
      
      let json: JSON = ["Address": address]
      return TGAddress(json: json)
   }
   
   struct JsonKeys {
      static let id = "id"
      static let address = "address"
      static let addressId = "address_id"
      static let city = "city"
      static let stateCode = "state_code"
      static let zip = "zip"
      static let latitude = "latitude"
      static let longitude = "longitude"
      static let dispatchType = "dispatch_type"
      static let dispatchUnits = "dispatch_unit_codes"
      static let url = "url"
      static let fullMessage = "full_msg"
      static let createdAt = "created_at"
   }
}

extension Alert {
   init(object: Any) throws {
      try self.init(json: JSON(object))
   }
   
   init(json: JSON) throws {
      id = json[JsonKeys.id].intValue
      address = json[JsonKeys.address].string
      addressId = json[JsonKeys.addressId].int
      latitude = json[JsonKeys.latitude].double
      longitude = json[JsonKeys.longitude].double
      
      dispatchType = json[JsonKeys.dispatchType].stringValue
      
      if let urlStr = json[JsonKeys.url].string {
         url = URL(string: urlStr)
      }
      else {
         url = nil
      }
      
      let dateStr = json[JsonKeys.createdAt].stringValue
      
      guard let date = Date(universalStr: dateStr) else {
         throw AppError.invalidJson(key: JsonKeys.createdAt)
      }
      
      createdAt = date
      
      city = json[JsonKeys.city].string
      stateCode = json[JsonKeys.stateCode].string
      zip = json[JsonKeys.zip].string
      dispatchUnits = json[JsonKeys.dispatchUnits].arrayValue.map { $0.stringValue }
      fullMessage = json[JsonKeys.fullMessage].string
   }
}
