//
//  WebViewController.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 7/25/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import UIKit
import Foundation
import RxCocoa
import RxSwift
import NVActivityIndicatorView

class WebViewController: UIViewController, UIWebViewDelegate, NVActivityIndicatorViewable {
   @IBOutlet weak var webView: UIWebView!
   
   var initialUrl: URL!
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.webView.delegate = self
      // Do any additional setup after loading the view.
      
      let loading = webView.rx.didStartLoad.take(1).do(onNext: { [unowned self] in
         self.startAnimating(
            CGSize(width: 200, height: 200),
            message: "Loading page content",
            type: .ballScaleRippleMultiple,
            color: UIColor(white: CGFloat(51) / 255, alpha: 1),
            padding: 40,
            backgroundColor: .clear
         )
      })
      
      let finished = Observable.from([
         webView.rx.didFinishLoad.map { Result<Void>.success(()) },
         webView.rx.didFailLoad.map { error -> Result<Void> in
            if (error as NSError).code == -999 {
               return Result<Void>.success(())
            }
            else {
               return Result<Void>.failure(AppError(error: error))
            }
         }
      ]).merge()

      Observable.combineLatest(loading, finished) { (_, failed)  in failed }
         .do(onNext: { [unowned self] _ in
            self.stopAnimating()
         })
         .filter { $0.value == nil }
         .subscribe(onNext: { _ in
            MessagesHandler.showError(
               message: "Unable to  load this page. Please, try to reopen it"
            )
         }).disposed(by: disposeBag)
      
      if let url = initialUrl {
         self.loadUrl(url: url)
      }
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      self.navigationController?.setNavigationBarHidden(false, animated: true)
   }
   
   func loadUrl(url: URL) {
      let request = URLRequest(url: url)
      
      let dataManager = ServiceLocator.dataManager
      let authedRequest = modify(
         request: request,
         token: dataManager.accessToken?.authToken ?? ""
      )
      
      log.info(authedRequest)
      webView.loadRequest(authedRequest)
   }
   
   func modify(request: URLRequest, token: String) -> URLRequest {
      var request = request
      request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
      return request
   }
   
   @IBAction func backAction(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
   }
   
   func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
      log.debug("Failed loading request")
      log.error(error)
   }
}
