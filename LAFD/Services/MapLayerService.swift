//
//  MapLayerService.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 9/8/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import TallyGoKit
import Alamofire
import RxCocoa
import RxSwift

protocol MapLayerService {
   func getHydrants(left: Double, right: Double, top: Double, bottom: Double)
      -> Observable<[Hydrant]>
   
   func getPreplans(left: Double, right: Double, top: Double, bottom: Double)
      -> Observable<[Preplan]>
 
   func getLayersSymbol() -> Single<[MapSymbol]>
}

class MapLayerServiceImpl: MapLayerService {
   let client: NetworkClient
   let dataManager: DataManagerType
   
   init(client: NetworkClient, dataManager: DataManagerType) {
      self.client = client
      self.dataManager = dataManager
   }
   
   func getHydrants(
      left: Double,
      right: Double,
      top: Double,
      bottom: Double) -> Observable<[Hydrant]> {
      
      let resource = Resource(
         route: .hydrants,
         method: .get,
         params: ["lng1": left, "lng2": right, "lat1": top, "lat2": bottom],
         encoding: URLEncoding.default,
         responseFormat: .json({ json in
            return json.arrayValue.map { Hydrant(json: $0) }
         })
      )
      
      let helper = UserAuthHelper(dataManager: self.dataManager)
      
      return self.client.send(resource: resource, requestHelper: helper)
   }
   
   func getPreplans(
      left: Double,
      right: Double,
      top: Double,
      bottom: Double) -> Observable<[Preplan]> {
      
      let resource = Resource(
         route: .preplanList,
         method: .get,
         params: ["lng1": left, "lng2": right, "lat1": top, "lat2": bottom],
         encoding: URLEncoding.default,
         responseFormat: .json({ json in
            return json.arrayValue.map { Preplan(json: $0) }
         })
      )
      
      let helper = UserAuthHelper(dataManager: self.dataManager)
      
      return self.client.send(resource: resource, requestHelper: helper)
   }
   
   func getLayersSymbol() -> Single<[MapSymbol]> {
      let resource = Resource(
         route: Route.symbols,
         method: .get,
         encoding: URLEncoding.default,
         responseFormat: .json({ json in
            try json.arrayValue.map { try MapSymbol(json: $0) }
         })
      )
      
      let helper = UserAuthHelper(dataManager: dataManager)
      
      return self.client.send(resource: resource, requestHelper: helper).asSingle()
   }
}
