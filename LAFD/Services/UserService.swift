//
//  UserService.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/28/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation

import Alamofire
import RxSwift

protocol UserService {
   func updateToken(token: String) -> Observable<Void>
   
   func removeToken(token: String) -> Observable<Void>
   
   func getUnitsLocation() -> Observable<[Unit]>
   
   func updateLocation(latitude: Double, longitude: Double) -> Observable<Void>
}

class UserServiceImpl: UserService {
   let client: NetworkClient
   let authHelper: RequestHelper
   
   init(client: NetworkClient, authHelper: RequestHelper) {
      self.client = client
      self.authHelper = authHelper
   }
   
   func updateToken(token: String) -> Observable<Void> {
      let resource = Resource(
         route: .updateToken,
         method: .post,
         params: ["app_token": token, "service": "apns"],
         responseFormat: .json({_ in })
      )
      
      return self.client.send(resource: resource, requestHelper: authHelper)
   }
   
   func removeToken(token: String) -> Observable<Void> {
      let resource = Resource(
         route: .removeToken,
         method: .delete,
         params: ["app_token": token, "service": "apns"],
         responseFormat: .json({_ in })
      )
      
      return self.client.send(resource: resource, requestHelper: authHelper)
   }
   
   func getUnitsLocation() -> Observable<[Unit]> {
      let resource = Resource(
         route: Route.trackLocations,
         method: .get,
         params: [:],
         encoding: URLEncoding.default,
         responseFormat: .json({ json in try json.arrayValue.map(Unit.init(json:)) })
      )
      
      return self.client.send(resource: resource, requestHelper: authHelper)
   }

   func updateLocation(latitude: Double, longitude: Double) -> Observable<Void> {
      let resource = Resource(
         route: .updateTrackingLocation,
         method: .post,
         params: ["lat": latitude, "lng": longitude],
         encoding: JSONEncoding.default,
         responseFormat: .json({ _ in })
      )
      
      return self.client.send(resource: resource, requestHelper: authHelper)
   }
}
