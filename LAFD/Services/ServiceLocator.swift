//
//  ServiceLocator.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/25/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation

enum ServiceLocator {
   static var dataManager: DataManagerType = {
      return DummyDataManager()
   }()
   
   static var networkClient: NetworkClient = {
      let helper = TokenValidationHelper(
         accountManager: ServiceLocator.accountManager
      )
      
      return NetworkClient(helper: helper)
   }()
   
   // MARK: Services
   static var alertService: AlertService = {
      let client = ServiceLocator.networkClient
      let dataManager = ServiceLocator.dataManager
      return AlertServiceImpl(client: client, dataManager: dataManager)
   }()
   
   static var authService: AuthService = {
      let client = ServiceLocator.networkClient
      let dataManager = ServiceLocator.dataManager
      
      let helper = StoreAccessTokenHelper(
         accountManager: ServiceLocator.accountManager
      )
      
      return AuthServiceImpl(client: client, accessTokenHelper: helper)
   }()
   
   static var userService: UserService = {
      let client = ServiceLocator.networkClient
      let dataManager = ServiceLocator.dataManager
      let helper =  UserAuthHelper(dataManager: dataManager)
      return UserServiceImpl(client: client, authHelper: helper)
   }()
   
   static var layerService: MapLayerService = {
      let client = ServiceLocator.networkClient
      let dataManager = ServiceLocator.dataManager
      return MapLayerServiceImpl(client: client, dataManager: dataManager)
   }()
   
   static var imagesDownloader: ImageDownloader = {
      let layerService = ServiceLocator.layerService
      return ImageDownloader(layerService: layerService)
   }()
   
   static var accountManager: AccountManager = {
      return AccountManager(dataManager: ServiceLocator.dataManager)
   }()
}
