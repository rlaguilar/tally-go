//
//  DataManager.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/28/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation

protocol DataManagerType {
   var accessToken: AccessToken? { get set }
   
   var deviceToken: String? { get set }
   
   var tokenSynced: Bool { get set }
   
//   var username: String? { get set }
//   
//   var password: String? { get set }
   
   var lastViewedAlertId: Int { get set }
}

class DummyDataManager: DataManagerType {
   private let defaults = UserDefaults.standard
   
//   var username: String? {
//      get { return defaults.string(forKey: #function) }
//      set { defaults.set(newValue, forKey: #function) }
//   }
//
//   var password: String? {
//      get { return defaults.string(forKey: #function) }
//      set { defaults.set(newValue, forKey: #function) }
//   }
   
   var deviceToken: String? {
      get { return defaults.string(forKey: #function) }
      set { defaults.set(newValue, forKey: #function) }
   }
   
   var tokenSynced: Bool {
      get { return defaults.bool(forKey: #function) }
      set { defaults.set(newValue, forKey: #function) }
   }
   
   var lastViewedAlertId: Int {
      get { return defaults.integer(forKey: #function) }
      set { defaults.set(newValue, forKey: #function) }
   }
   
   var accessToken: AccessToken? {
      get {
         guard let data = defaults.data(forKey: #function) else {
            return nil
         }
         
         let accessToken = AccessToken(object: data)
         return accessToken
      }
      
      set {
         let data: Data?
         
         if let accessToken = newValue {
            data = try? JSONSerialization.data(
               withJSONObject: accessToken.dictionaryRepresentation(),
               options: []
            )
         }
         else {
            data = nil
         }
         
         defaults.set(data, forKey: #function)
      }
   }
}
