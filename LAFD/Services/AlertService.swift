//
//  AlertService.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 9/1/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

protocol AlertService {
   func retrieve(afterId: Int?, beforeId: Int?, count: Int) -> Observable<[Alert]>
   
   func getStory(alertId: Int) -> Observable<SizeupStory>
}

class AlertServiceImpl: AlertService {
   let client: NetworkClient
   let dataManager: DataManagerType
   
   init(client: NetworkClient, dataManager: DataManagerType) {
      self.client = client
      self.dataManager = dataManager
   }
   
   func retrieve(afterId: Int?, beforeId: Int?, count: Int) -> Observable<[Alert]> {
      let sinceId = afterId?.advanced(by: 1)
      let maxId = beforeId?.advanced(by: -1)
      
      var params: [String: Any] = ["page_size": count]
      
      if let sinceId = sinceId {
         params["since_id"] = sinceId
      }
      
      if let maxId = maxId {
         params["max_id"] = maxId
      }
      
      let resource = Resource(
         route: .getAlerts,
         method: .get,
         params: params,
         encoding: URLEncoding.default,
         responseFormat: .json({ json in
            return json.arrayValue.map { try? Alert(json: $0) }.filter { $0 != nil }.map { $0! }
         })
      )
      
      let helper = UserAuthHelper(dataManager: self.dataManager)
      
      return self.client.send(resource: resource, requestHelper: helper)
   }
   
   func getStory(alertId id: Int) -> Observable<SizeupStory> {
      let resource = Resource(
         route: .story(alertId: id),
         method: .get,
         encoding: URLEncoding.default,
         responseFormat: .json({ json in
            return try SizeupStory(json: json)
         })
      )
      
      let helper = UserAuthHelper(dataManager: self.dataManager)
      return self.client.send(resource: resource, requestHelper: helper)
   }
}
