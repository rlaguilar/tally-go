//
//  AuthService.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/25/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import RxSwift

protocol AuthService {
   func signIn(email: String, password: String) -> Observable<AccessToken>
}

class AuthServiceImpl: AuthService {
   let client: NetworkClient
   let accessTokenHelper: RequestHelper
   
   init(client: NetworkClient, accessTokenHelper: RequestHelper) {
      self.client = client
      self.accessTokenHelper = accessTokenHelper
   }
   
   func signIn(email: String, password: String) -> Observable<AccessToken> {
      let resource = Resource(
         route: .login,
         method: .post,
         params: [
            "email": email,
            "password": password,
            "grant_type": "client_credentials"
         ],
         responseFormat: .json(AccessToken.init)
      )
      
      return self.client.send(resource: resource, requestHelper: accessTokenHelper)
   }
}
