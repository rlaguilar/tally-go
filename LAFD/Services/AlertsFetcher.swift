//
//  AlertsFetcher.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 9/13/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import RxSwift
import Reachability

class AlertsFetcher {
   var timeline: Observable<Result<[Alert]>>
   let disposeBag = DisposeBag()
   
   let isLoading: Variable<Bool>
   
   let manualTrigger = PublishSubject<Void>()
   
   init(alertService: AlertService, pageSize: Int) {
      let triggerObservable = Observable.from([
         self.manualTrigger.asObservable(),
         NotificationCenter.default.rx.notification(.UIApplicationDidBecomeActive)
            .map { _ in },
         NotificationCenter.default.rx.notification(RemoteNotificationArrived)
            .map { _ in }
         ]).merge()
         .debounce(0.5, scheduler: MainScheduler.instance)
         .startWith(())
      
      triggerObservable.subscribe().disposed(by: disposeBag)
      
      let feedCursor = Variable<TimelineCursor>(.none)
      
      let isLoading = Variable(false)
   
      let allItems: Observable<Result<[Alert]>> = Observable.combineLatest(
         triggerObservable,
         Reactive<Reachability>.reachable
      ) { ($0, $1) }
         .filter { (_, reachable) in reachable }
         .map { (trigger, _) in trigger }
         .withLatestFrom(feedCursor.asObservable())
         .do(onNext: { _ in isLoading.value = true })
         .flatMapLatest { cursor -> Observable<Result<[Alert]>> in
            return alertService.retrieve(
               afterId: cursor.maxId,
               beforeId: nil,
               count: pageSize
            ).map { .success($0) }
            .catchError({ (error) in .just(.failure(AppError(error: error)))})
         }.do(
            onNext: { _ in isLoading.value = false },
            onError: { _ in isLoading.value = false }
         ).share(replay: 1, scope: SubjectLifetimeScope.forever)
      
      self.isLoading = isLoading
      
      timeline = allItems.map { result -> Result<[Alert]> in
         switch result {
         case .success(let items):
            let prev24Hours = Date().addingTimeInterval(-24*60*60)
            let newItems = items.filter { $0.createdAt >= prev24Hours }
            return .success(newItems)
         case .failure:
            return result
         }
      }
      
      allItems.map { result -> [Alert] in
         switch result {
         case .success(let items): return items
         default: return []
         }
      }.scan(TimelineCursor.none) { (currentCursor, alerts) -> TimelineCursor in
         return alerts.reduce(currentCursor, { (cursor, alert) in
             TimelineCursor(maxId: max(cursor.maxId, alert.id))
         })
      }.bind(to: feedCursor).disposed(by: disposeBag)
   }
}

struct TimelineCursor {
   let maxId: Int
   
   // TODO: Integrate since Id
   static let none = TimelineCursor(maxId: 0)
}
