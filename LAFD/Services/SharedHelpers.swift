//
//  SharedHelpers.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 9/1/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation

class UserAuthHelper: RequestHelper {
   let dataManager: DataManagerType
   
   var headers: [String : String] {
      let token = dataManager.accessToken?.authToken ?? ""
      return ["Authorization": "Bearer \(token)"]
   }
   
   init (dataManager: DataManagerType) {
      self.dataManager = dataManager
   }
}

class StoreAccessTokenHelper: RequestHelper {
   let accountManager: AccountManager
   
   init(accountManager: AccountManager) {
      self.accountManager = accountManager
   }
   
   func afterSuccess(value: Any) {
      guard let token = value as? AccessToken else { return }
      self.accountManager.update(token: token)
   }
}

class TokenValidationHelper: RequestHelper {
   let accountManager: AccountManager
   
   init(accountManager: AccountManager) {
      self.accountManager = accountManager
   }
   
   func afterError(error: Error, code: Int?) {
      if code == 401 {
         accountManager.update(token: nil)
      }
   }
}
