//
//  ReactiveViewController.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 10/9/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ReactiveViewController: UIViewController {
   let _whenViewAppeared = ReplaySubject<Void>.create(bufferSize: 1)
   lazy var whenViewAppeared = self._whenViewAppeared.asObservable()
   
   override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)
      _whenViewAppeared.onNext(())
   }
}
