//
//  AlertStatusViewController.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 10/10/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

enum AlertResponseStatus {
   case unknown
   case responding
   case arrived
   case unavailable
}

class AlertStatusViewController: UIViewController {
   @IBOutlet weak var respondingButton: UIButton!
   @IBOutlet weak var arrivedButton: UIButton!
   @IBOutlet weak var unavailableButton: UIButton!
   
   @IBOutlet weak var backgroundView: UIView!
   
   lazy var selectedStatus = {
      Observable.of(
         self.respondingButton.rx.tap.map { AlertResponseStatus.responding },
         self.arrivedButton.rx.tap.map { AlertResponseStatus.arrived },
         self.unavailableButton.rx.tap.map { AlertResponseStatus.unavailable }
      ).merge()
   }()
   
   override func viewDidLoad() {
      super.viewDidLoad()
      backgroundView.layer.cornerRadius = 8
      
      let tap = UITapGestureRecognizer()
      
      tap.rx.event.bind { [unowned self] touch in
         let location = touch.location(in: self.backgroundView)
         
         if !self.backgroundView.bounds.contains(location) {
            self.dismiss(animated: true, completion: nil)
         }
      }.disposed(by: disposeBag)
      
      view.addGestureRecognizer(tap)
   }
}
