//
//  BaseViewController.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/25/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

func notify(error: AppError) {
   NotificationCenter.default.post(
      name: .NewErrorNotification,
      object: nil,
      userInfo: [NewErrorObjectKey: error]
   )
}

extension UIView {
   func installDismissKeyboardRecognizer() {
      let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard(gesture:)))
      tap.cancelsTouchesInView = false
      self.addGestureRecognizer(tap)
   }
   
   @objc private func dismissKeyboard(gesture: UIGestureRecognizer) {
      self.endEditing(true)
   }
}

extension UIApplication {
   class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
      if let nav = base as? UINavigationController {
         return topViewController(base: nav.visibleViewController)
      }
      if let tab = base as? UITabBarController {
         if let selected = tab.selectedViewController {
            return topViewController(base: selected)
         }
      }
      if let presented = base?.presentedViewController {
         return topViewController(base: presented)
      }
      
      return base
   }
}

extension UIViewController {
   /// Blocks the UI while executing a long running task
   ///
   /// - Parameters:
   ///   - task: Operation that will run while the UI is blocked
   ///   - completion: Handler to call once the task finished running
   /// - Returns: The value returned by the task when invoked
   @discardableResult
   func performLongRunningUITask<T, U>(task: (@escaping (T) -> Void) -> U, completion: @escaping (T) -> Void) -> U {
      
      let loadingVC = LoadingViewController()
      self.present(loadingVC, animated: true, completion: nil)
      
      return task { (result) in
         self.dismiss(animated: false, completion: {
            completion(result)
         })
      }
   }
   
   func performTask<T>(observable: Observable<T>) -> Observable<T> {
      return Observable.create({ (observer) -> Disposable in
         let disposable = observable.subscribe(
            onNext: { (item) in
               self.dismiss(animated: false, completion: {
                  observer.onNext(item)
                  // TODO: change observable semantics here
                  observer.onCompleted()
               })
            },
            onError: { (error) in
               self.dismiss(animated: false, completion: {
                  observer.onError(error)
               })
            }
         )
         
         return Disposables.create {
            disposable.dispose()
         }
      }).do(onSubscribe: {
         let loadingVC = LoadingViewController()
         self.present(loadingVC, animated: true, completion: nil)
      })
      
//      let loadingVC = LoadingViewController()
//      self.present(loadingVC, animated: true, completion: nil)
//      
//      return observable.do(
//         onNext: { (_) in
//         self.dismiss(animated: false, completion: nil)
//         },
//         onError: { (_) in
//            self.dismiss(animated: false, completion: nil)
//         })
   }
}
