//
//  HomeViewController.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/22/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import UIKit
import RxSwift

let NotificationShowLeftPanel = Notification.Name("NotificationShowLeftPanel")

class HomeViewController: SplitViewController {
   var dataManager: DataManagerType
   
   let userService: UserService
   
   let incidentsListVC: IncidentsListViewController
   
   init(dataManager: DataManagerType, userService: UserService, alertService: AlertService) {
      self.dataManager = dataManager
      self.userService = userService
      
      let storyboard = UIStoryboard(name: "Main", bundle: .main)
      let middleController = storyboard.instantiateViewController(withIdentifier: "ViewController") as? ViewController
      let rightController = storyboard.instantiateViewController(withIdentifier: "OptionsViewController") as? OptionsViewController
      
      if let middle = middleController, let right = rightController {
         
         self.incidentsListVC = IncidentsListViewController(alertService: alertService)
         
         super.init(
            leftVC: UINavigationController(rootViewController: self.incidentsListVC),
            middleVC: middle,
            rightVC: right
         )
      }
      else {
         fatalError()
      }
   }
   
   required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      let center = NotificationCenter.default
      
      center.rx.notification(NotificationShowLeftPanel)
         .subscribe(onNext: { [weak self]  _ in
            guard let `self` = self else { return }
            // FIXME: This assumes this view controller to be the first in the
            // navigation view controller and also assumes all controllers are
            // pushen in that navigation controller
            if UIApplication.topViewController() != self {
               self.navigationController?.popToRootViewController(animated: false)
            }
            
            log.debug("Received new notification here")
            // FIXME: add logic to handle panels visibility
            if !self.leftToggleButton.isUserInteractionEnabled {
               self.showRightPanel(show: false, animated: false)
            }
            
            self.showLeftPanel(show: true, animated: false)
         }).disposed(by: disposeBag)
      
      if AppDelegate.instance.showLeftPanel {
         self.showLeftPanel(show: true, animated: false)
      }
      
      Observable.of(
         incidentsListVC.unreadItems.map { _ in },
         whenViewAppeared,
         center.rx.notification(.UIApplicationDidBecomeActive).map { _ in }
      ).merge()
         .withLatestFrom(incidentsListVC.unreadItems)
         .map { !$0.isEmpty }
         .subscribe(onNext: { [unowned self] hasUnreadItems in
            if !self.isLeftOpened {
               self.animateLeftButton(animate: hasUnreadItems)
            }
         }).disposed(by: disposeBag)
      
      guard let deviceToken = dataManager.deviceToken,
            !dataManager.tokenSynced else {
               return
      }
      
      userService.updateToken(token: deviceToken)
         .subscribe(onNext: { [unowned self] in
            self.dataManager.tokenSynced = true
         }, onError: { (error) in
            print("Error syncing the device token: \(error)")
         }).disposed(by: self.disposeBag)
   }
   
   func animateLeftButton(animate: Bool) {
      self.leftToggleButton.layer.removeAllAnimations()
      self.leftToggleButton.backgroundColor = UIColor(
         red: 229.0 / 255,
         green: 32.0 / 255,
         blue: 4.0 / 255, alpha: 1
      )
      
      if animate {
         UIView.animate(withDuration: 0.2, delay: 0, options: [.repeat, .autoreverse, .allowUserInteraction], animations: { [weak self] in
            self?.leftToggleButton.backgroundColor = UIColor.black
         }, completion: nil)
      }
   }
   
   override func showLeftPanel(show: Bool, animated: Bool) {
      animateLeftButton(animate: false)
      
      if !show && isLeftOpened {
         incidentsListVC.markAllAsRead()
      }
      
      super.showLeftPanel(show: show, animated: animated)
   }
}
