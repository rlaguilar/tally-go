//
//  AlertDetailsViewController.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 10/4/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import UIKit
import RxSwift
import FirebaseAnalytics

class AlertDetailsViewController: ReactiveViewController {
   @IBOutlet weak var scrollView: UIScrollView!
   @IBOutlet weak var incidentNumberLabel: UILabel!
   @IBOutlet weak var incidentAddress: UILabel!
   @IBOutlet weak var incidentCrossStreetLabel: UILabel!
   @IBOutlet weak var dateLabel: UILabel!
   
   @IBOutlet weak var statusButton: UIButton!
   
   @IBOutlet weak var incidentTypeLabel: UILabel!
   @IBOutlet weak var unitsLabel: UILabel!
   
   @IBOutlet weak var callNotesLabel: UILabel!
   @IBOutlet weak var sizeupLabel: UILabel!
   
   @IBOutlet weak var sizeupButton: UIButton!
   @IBOutlet weak var rollButton: UIButton!
   
   var item: AlertViewModel!
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      // Do any additional setup after loading the view.
      self.navigationItem.backBarButtonItem?.title = ""
      let alert = item.alert
      incidentNumberLabel.text = "Incident: \(alert.id)"
      dateLabel.text = alert.createdAt.formatToReadable()
      
      incidentAddress.text = alert.address
      incidentCrossStreetLabel.text = alert.city != nil ? "@ \(alert.city!)" : nil
      incidentCrossStreetLabel.text = incidentCrossStreetLabel.text.toRequired()
      callNotesLabel.text = alert.fullMessage.toRequired()
//      sizeupLabel.text = alert.story.toRequired()
      unitsLabel.text = alert.dispatchUnits.joined(separator: ", ").toRequired()
      incidentTypeLabel.text = alert.dispatchType.toRequired()
      
      let alertService = ServiceLocator.alertService
      
      if let addressId = self.item.alert.addressId {
         alertService.getStory(alertId: addressId).map { $0.story }
            .startWith("Loading story")
            .bind(to: sizeupLabel.rx.text)
            .disposed(by: disposeBag)
      }
      else {
         sizeupLabel.text = "---"
      }
      
      
      sizeupButton.rx.tap.bind { [unowned self] in
         AnalyticsEvent.showSizeup.log(params: [
            AnalyticsParameterItemID: alert.id
         ])
         
         NotificationCenter.default.post(
            name: ShowSizeupNotification,
            object: nil,
            userInfo: [AlertNotificationKey: self.item.alert]
         )
      }.disposed(by: self.disposeBag)
      
      self.whenViewAppeared.bind { [unowned self] in
         var insets = self.scrollView.contentInset
         
         // TODO: This value is to allow the scroll to scroll beyond the incidents
         // button
         insets.bottom = 112
         self.scrollView.contentInset = insets
         self.scrollView.scrollIndicatorInsets = insets
         }.disposed(by: disposeBag)
      
      rollButton.rx.tap.bind { [unowned self] in
         AnalyticsEvent.rollTo.log(params: [
            AnalyticsParameterItemID: alert.id
         ])
         NotificationCenter.default.post(
            name: RollToAlertNotification,
            object: nil,
            userInfo: [AlertNotificationKey: self.item.alert]
         )
      }.disposed(by: self.disposeBag)
      
      statusButton.rx.tap.bind { [unowned self] in
         let controller = AlertStatusViewController.instantiate(from: .Main)
         controller.modalPresentationStyle = .overCurrentContext
         controller.modalTransitionStyle = .crossDissolve
         self.present(controller, animated: true, completion: nil)
         
         controller.selectedStatus.bind(to: self.item.status)
            .disposed(by: controller.disposeBag)
         
         controller.selectedStatus.take(1).subscribe(onNext: { [unowned controller] _ in
            controller.dismiss(animated: true, completion: nil)
         }).disposed(by: controller.disposeBag)
      }.disposed(by: disposeBag)
      
      item.status.asObservable().map { status -> String in
         switch status {
         case .unknown:
            return "Status"
         case AlertResponseStatus.arrived:
            return "Arrived"
         case AlertResponseStatus.responding:
            return "Responding"
         case AlertResponseStatus.unavailable:
            return "Unavailable"
         }
      }.bind(to: self.statusButton.rx.title())
         .disposed(by: disposeBag)
      
      rollButton.isHidden = alert.coordinates == nil
      sizeupButton.isHidden = alert.url == nil
   }
}
