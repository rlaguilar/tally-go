//
//  OptionsViewController.swift
//  LAFD
//
//  Created by Anthony Picciano on 11/18/16.
//  Copyright © 2016 TALLYGO. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class OptionsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, AdjustableTableHeight {
   
   @IBOutlet weak var tableView: UITableView!
   
   let settingsIndex = 0
   let notificationIndex = 1
   let preplanIndex = 2
   let newFeaturesIndex = 3
   let devToolsIndex = 4
   let logoutIndex = 5
   let drivingSimulatorIndex = 6
   
   let items = ["Settings", "Notifications", "Pre-Plans", "Enhancement requests", "Dev Info", "Logout", "Driving Simulator"]
   let itemImages = [ #imageLiteral(resourceName: "ic_settings"), #imageLiteral(resourceName: "ic_notifications"), #imageLiteral(resourceName: "ic_preplan"), #imageLiteral(resourceName: "ic_new_features"), #imageLiteral(resourceName: "ic_dev_tools"), #imageLiteral(resourceName: "ic_logout"), LAFDStyleKit.imageOfDrivingSimulatorIcon ]
   
   let userService = ServiceLocator.userService
   let accountManager = ServiceLocator.accountManager
   var dataManager = ServiceLocator.dataManager
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }
   
   override func viewDidLoad() {
      super.viewDidLoad()
   }
   
   //MARK: - Table view data source and delegate methods
   
   override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
      adjustTableHeight()
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      if UserDefaults.standard.bool(forKey: "allowDrivingSimulation") {
         return items.count
      }
      
      return items.count - 1
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "Options Cell", for: indexPath)
      cell.textLabel?.text = items[indexPath.row]
      cell.imageView?.image = itemImages[indexPath.row]
      cell.tintColor = UIColor.white
      return cell
   }
   
   func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      cell.separatorInset = .zero
      cell.textLabel?.font = UIFont.systemFont(ofSize: 18)
      cell.textLabel?.textColor = .white
   }
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      tableView.deselectRow(at: indexPath, animated: true)
      
      switch indexPath.row {
      case settingsIndex:
         AnalyticsEvent.showUserSettings.log()
         let url = Route.settings.absoluteUrl
         let vc = WebViewController.instantiate(from: .Main)
         vc.initialUrl = url
         self.show(vc, sender: self)
         self.navigationController?.setNavigationBarHidden(false, animated: true)
      case notificationIndex:
         log.info("TODO: Handle notification link")
         
      case preplanIndex:
         AnalyticsEvent.showPreplans.log()
         let url = Route.preplanWeb.absoluteUrl
         let vc = WebViewController.instantiate(from: .Main)
         vc.initialUrl = url
         self.show(vc, sender: self)
         self.navigationController?.setNavigationBarHidden(false, animated: true)
         
      case newFeaturesIndex:
         AnalyticsEvent.showCommingFeatures.log()
         let url = Route.commingSoonFeatures.absoluteUrl
         let vc = WebViewController.instantiate(from: .Main)
         vc.initialUrl = url
         self.show(vc, sender: self)
         self.navigationController?.setNavigationBarHidden(false, animated: true)
         
      case devToolsIndex:
         let vc = DevInfoViewController.instantiate(from: .Main)
         self.show(vc, sender: self)
         self.navigationController?.setNavigationBarHidden(false, animated: true)
      case logoutIndex:
         self.logout()
      case drivingSimulatorIndex:
         performSegue(withIdentifier: items[indexPath.row], sender: self)
         self.navigationController?.setNavigationBarHidden(false, animated: true)
      default:
         fatalError("Invalid selected index here")
         
      }
   }
   
   func logout() {
      // FIXME: Move Logout to other place (i.e Account Manager). Note that it's wrong, you should
      // remove the Device token from listening notifications, no
      // the access token.
      AnalyticsEvent.logout.log()
      let deviceToken = self.dataManager.deviceToken ?? ""
      self.userService.removeToken(token: deviceToken)
         .subscribe(onNext: { [unowned self] in
            self.accountManager.update(token: nil)
            self.dataManager.lastViewedAlertId = 0
      }).disposed(by: disposeBag)
   }
}
