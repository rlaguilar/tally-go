//
//  AppDelegate+MessageHandling.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/25/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import UIKit

extension AppDelegate: MessagesNotificationHandler {
   func startListeningMessages() {
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(processErrorMessage(notification:)),
         name: .NewErrorNotification,
         object: nil
      )
   }
   
   func stopListeningMessages() {
      NotificationCenter.default.removeObserver(
         self,
         name: .NewErrorNotification,
         object: nil
      )
   }
   
   @objc func processErrorMessage(notification: Notification) {
      guard let error = notification.userInfo?[NewErrorObjectKey] as? AppError,
         let controller = UIApplication.topViewController() else {
            return
      }
      
      let alert = UIAlertController(title: "Error", message: error.description, preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
      
      controller.present(alert, animated: true, completion: nil)
   }
}
