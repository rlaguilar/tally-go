//
//  LoginViewController.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/22/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import UIKit
import RxSwift

class LoginViewController: UIViewController {
   @IBOutlet weak var usernameField: UITextField!
   @IBOutlet weak var passwordField: UITextField!
   @IBOutlet weak var loginButton: UIButton!
   
   @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
   @IBOutlet weak var wrapperView: UIView!
   
   var authService: AuthService!
   
   static func createInstance(authService: AuthService) -> LoginViewController {
      let controller = LoginViewController.instantiate(from: .Main)
      controller.authService = authService
      return controller
   }
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      // Do any additional setup after loading the view.
      self.loginButton.addTarget(
         self,
         action: #selector(self.login(_:)),
         for: .touchUpInside
      )
      
      self.view.installDismissKeyboardRecognizer()
      
      usernameField.delegate = self
      passwordField.delegate = self
      
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(self.keyboardDidShow(notification:)),
         name: Notification.Name.UIKeyboardWillShow,
         object: nil
      )
      
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(self.keyboardDidHide(notification:)),
         name: Notification.Name.UIKeyboardWillHide,
         object: nil
      )
   }
   
   @objc func login(_ sender: Any) {
      let username = usernameField.text ?? ""
      let password = passwordField.text ?? ""
      
      let task = authService.signIn(
         email: username,
         password: password
      )
      
      self.performTask(observable: task).subscribe(onNext: { (user) in
         AnalyticsEvent.login.log()
         
         let dataManager = ServiceLocator.dataManager
         
         let homeController = HomeViewController(
            dataManager: dataManager,
            userService: ServiceLocator.userService,
            alertService: ServiceLocator.alertService
         )

         homeController.modalTransitionStyle = .crossDissolve
         let navigation = UINavigationController(rootViewController: homeController)
         
         self.present(navigation, animated: true, completion: nil)
      }, onError: { (error) in
         notify(error: error as! AppError)
      }).disposed(by: disposeBag)
   }
   
   @objc func keyboardDidShow(notification: Notification) {
      guard let keyboardFrame = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect else {
         return
      }
      
      guard let rect = AppDelegate.instance.window?.convert(keyboardFrame, from: nil) else {
         return
      }
      
      self.wrapperView.transform = CGAffineTransform.identity
      let height = rect.intersection(self.wrapperView.frame).height
      let padding: CGFloat = 12
      self.wrapperView.transform = CGAffineTransform(translationX: 0, y: -(height + padding))
   }
   
   @objc func keyboardDidHide(notification: Notification) {
      self.wrapperView.transform = CGAffineTransform.identity
   }
   
   override var preferredStatusBarStyle: UIStatusBarStyle {
      return UIStatusBarStyle.default
   }
   
   override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
      if UIDevice.current.userInterfaceIdiom == .pad {
         return UIInterfaceOrientationMask.all
      }
      else {
         return UIInterfaceOrientationMask.portrait
      }
   }
}

extension LoginViewController: UITextFieldDelegate {
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      switch textField {
      case usernameField:
         passwordField.becomeFirstResponder()
      case passwordField:
         passwordField.resignFirstResponder()
         self.login(textField)
      default:
         fatalError("Unrecognized field")
      }
      
      return true
   }
}
