//
//  LoadingViewController.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/25/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import UIKit

class LoadingViewController: UIViewController {
   
   init() {
      super.init(nibName: nil, bundle: nil)
      
      self.modalPresentationStyle = .overFullScreen
      self.modalTransitionStyle = .crossDissolve
   }
   
   required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      // Do any additional setup after loading the view.
      let indicatorView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
      indicatorView.startAnimating()
      self.view.addSubview(indicatorView)
      indicatorView.center = self.view.center
      
      indicatorView.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleTopMargin, .flexibleBottomMargin]
      
      self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
   }
}
