//
//  TGAddress+Display.swift
//  LAFD
//
//  Created by Anthony Picciano on 12/14/16.
//  Copyright © 2016 TALLYGO. All rights reserved.
//

import TallyGoKit

extension TGAddress {
    
    var displayAddress: String {
        var result = String()
        
        if let address = address {
            result.append(address)
        }
        
        if let city = city {
            result.append(", ")
            result.append(city)
        }
        
        if let state = region {
            result.append(", ")
            result.append(state.replacingOccurrences(of: "California", with: "CA"))
        }
        
        return result
    }
    
}
