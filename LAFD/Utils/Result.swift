//
//  Result.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/24/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation

enum Result<T> {
   case success(T)
   case failure(AppError)
   
   var value: T? {
      switch self {
      case .success(let value):
         return value
      case .failure: return nil
      }
   }
   func map<U>(mapper: (T) -> U) -> Result<U> {
      switch self {
      case .failure(let error):
         return Result<U>.failure(error)
      case .success(let value):
         return Result<U>.success(mapper(value))
      }
   }
}
