//
//  AppStoryboard.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/25/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import UIKit

enum AppStoryboard: String {
   case Main
   
   var instance: UIStoryboard {
      return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
   }
   
   func viewController<Controller: UIViewController>(
      fromType: Controller.Type,
      function: String = #function,
      line: Int = #line,
      file: String = #file
      ) -> Controller {
      
      let storyboardID = Controller.storybaordID
      
      guard let item = instance.instantiateViewController(withIdentifier: storyboardID) as? Controller else {
         fatalError("\(file): \(line) \(function) Identifier \(storyboardID) not found in \(self.rawValue) storyboard")
      }
      
      return item
   }
}

extension UIViewController {
   class var storybaordID: String {
      return "\(self)"
   }
   
   static func instantiate(
      from appStoryboard: AppStoryboard,
      function: String = #function,
      line: Int = #line,
      file: String = #file
      ) -> Self {
      
      return appStoryboard.viewController(fromType: self, function: function, line: line, file: file)
   }
}
