//
//  AppError.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/24/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation

enum AppError: Error, CustomStringConvertible {
   case message(String)
   case generic(Error)
   case invalidJson(key: String)
   case invalidOperation(message: String)
   
   init(message: String) {
      self = .message(message)
   }
   
   init(error: Error) {
      if let appError = error as? AppError {
         self = appError
      }
      else {
         self = .generic(error)
      }
   }
   
   var description: String {
      switch self {
      case .message(let msg):
         return msg
      case .generic(let error):
         return error.localizedDescription
      case .invalidJson(let key):
         return "Unable to retrieve key `\(key)` from the json"
      case .invalidOperation(let message):
         return message
      }
   }
}

extension AppError {
   static func wrap<T>(block: () throws -> T) throws -> T {
      do {
         return try block()
      } catch {
         throw AppError(error: error)
      }
   }
   
   static func attempt<T>(block: () throws -> T) -> T? {
      do {
         return try wrap(block: block)
      } catch {
         print(error)
         return nil
      }
   }
}
