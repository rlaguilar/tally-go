//
//  MessagesHandler.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 9/15/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import SwiftMessages

class MessagesHandler {
   class func showInfo(message: String) {
      let view = MessageView.viewFromNib(layout: .cardView)
      view.configureTheme(.warning)
      view.configureDropShadow()
      view.configureContent(
         title: "Warning",
         body: message,
         iconImage: nil,
         iconText: "",
         buttonImage: nil,
         buttonTitle: "Show") { _ in
            NotificationCenter.default.post(name: NotificationShowLeftPanel, object: nil)
            SwiftMessages.hide()
         }
      
      // TODO: add support for canceling the operation
//      view.button?.isHidden = true
      var config = SwiftMessages.defaultConfig
      config.presentationContext = .window(windowLevel: UIWindowLevelAlert)
      
//      config.duration = .indefinite(delay: 1, minimum: 1)
      config.duration = .seconds(seconds: 3)
      config.presentationStyle = .top
//      config.dimMode = .gray(interactive: false)
      config.dimMode = .none
//      config.interactiveHide = false
      config.interactiveHide = true
      view.adjustWidth()
      SwiftMessages.show(config: config, view: view)
   }
   
   class func showError(message: String, title: String = "Error") {
      let view = MessageView.viewFromNib(layout: .cardView)
      view.configureTheme(.error)
      view.configureDropShadow()
      view.configureContent(title: title, body: message, iconText: "🤡")
      // TODO: add support for canceling the operation
      view.button?.isHidden = true
      var config = SwiftMessages.defaultConfig
      config.presentationContext = .automatic
//      config.presentationContext = .window(windowLevel: UIWindowLevelAlert)
      
      //      config.duration = .indefinite(delay: 1, minimum: 1)
      config.duration = .seconds(seconds: 3)
      config.presentationStyle = .top
      //      config.dimMode = .gray(interactive: false)
      config.dimMode = .none
      //      config.interactiveHide = false
      config.interactiveHide = true
      view.adjustWidth()
      SwiftMessages.show(config: config, view: view)
   }
   
   class func showProgress(message: String) -> String {
      let id = UUID().uuidString
      let view = MessageView.viewFromNib(layout: .cardView)
      view.id = id
      view.configureTheme(.info)
      view.configureDropShadow()
      view.configureContent(title: "Info", body: message, iconText: "⏰")
      view.button?.isHidden = true
      var config = SwiftMessages.defaultConfig
      config.presentationContext = .window(windowLevel: UIWindowLevelAlert)
      
      config.duration = .indefinite(delay: 0.1, minimum: 0.5)
      config.presentationStyle = .center
      config.dimMode = .gray(interactive: false)
      config.interactiveHide = false
      view.adjustWidth()
      SwiftMessages.show(config: config, view: view)
      return id
   }
   
   class func hideProgress(with id: String) {
      SwiftMessages.hide(id: id)
   }
   
   let networkTroublesViewId = "networkTroublesView"
   
   let networkConnectionMessage = SwiftMessages()
   
   func showNetworkTroublesIndicator(show: Bool) {
      networkConnectionMessage.hide(id: networkTroublesViewId)
      
      guard show else { return }
      
      let view = MessageView.viewFromNib(layout: .statusLine)
      view.id = networkTroublesViewId
      view.configureTheme(.error)
      view.configureContent(body: "Network connection unavailable")
      
      var config = networkConnectionMessage.defaultConfig
      config.duration = .indefinite(delay: 1, minimum: 1)
      config.presentationStyle = .bottom
      config.dimMode = .none
      config.interactiveHide = false
      networkConnectionMessage.show(config: config, view: view)
   }
}

extension MessageView {
   func adjustWidth() {
      if UIDevice.current.userInterfaceIdiom == .pad {
         self.configureBackgroundView(width: 400)
      }
   }
}
