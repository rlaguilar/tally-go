//
//  AppNotifications.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/25/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation

/// Protocol that should be conformed by objects that will handle
/// app level info and error messages
/// - TODO: Define a better interface that should put clearer the messages to
///         listen for.
protocol MessagesNotificationHandler: class {
   func startListeningMessages()
   
   func stopListeningMessages()
}

extension NSNotification.Name {
   /// Indicates that a new network connection has started
   public static let NetworkConnectionStarted = NSNotification.Name("NetworkConnectionStarted")
   
   /// Indicates that a previously active network connection has finished
   public static let NetworkConnectionFinished = NSNotification.Name("NetworkConnectionFinished")
   
   public static let NewErrorNotification = NSNotification.Name("NewError")
   
   public static let NewInfoMessageNotification = NSNotification.Name("InfoMessage")
}

let NewErrorObjectKey = "NewErrorObject"
let NewInfoMessageKey = "NewInfoMessageKey"
