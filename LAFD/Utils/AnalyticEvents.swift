//
//  AnalyticEvents.swift
//  First Due Mobile
//
//  Created by Reynaldo Aguilar on 10/27/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import FirebaseAnalytics

enum AnalyticsEvent: String {
   case rollTo = "roll_to_incident"
   case showSizeup = "show_sizeup_page"
   case showUserSettings = "show_user_settings_page"
   case showPreplans = "show_preplans_page"
   case showCommingFeatures = "enhancement_requests"
   case login = "login"
   case logout = "logout"
   
   func log(params: [String: Any]? = nil) {
      Analytics.logEvent(self.rawValue, parameters: params)
   }
}
