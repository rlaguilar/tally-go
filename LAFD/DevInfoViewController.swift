//
//  DevInfoViewController.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 10/19/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import UIKit

class DevInfoViewController: UIViewController {
   @IBOutlet weak var tokenField: UITextView!
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      // Do any additional setup after loading the view.
      let dataManager = ServiceLocator.dataManager
      tokenField.text = dataManager.deviceToken
   }
   
   override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
   }
   
   
   /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
   
}
