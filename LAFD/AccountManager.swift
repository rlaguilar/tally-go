//
//  AccountManager.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 10/18/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

typealias LiveToken = Driver<TokenStatus>

enum TokenStatus {
   case unauthorized
   case authorized(AccessToken)
   
   var token: AccessToken? {
      switch self {
      case .unauthorized:
         return nil
      case .authorized(let token):
         return token
      }
   }
}

class AccountManager {
   private var dataManager: DataManagerType
   
   private let _current = ReplaySubject<TokenStatus>.create(bufferSize: 1)
   
   lazy var current: LiveToken = self._current.asDriver(onErrorJustReturn: .unauthorized)
   
   init(dataManager: DataManagerType) {
      self.dataManager = dataManager
      
      if let token = dataManager.accessToken {
         _current.onNext(.authorized(token))
      }
      else {
         _current.onNext(.unauthorized)
      }
   }
   
   func update(token: AccessToken?) {
      dataManager.accessToken = token
      
      if let token = token {
         _current.onNext(.authorized(token))
      }
      else {
         _current.onNext(.unauthorized)
      }
   }
}
