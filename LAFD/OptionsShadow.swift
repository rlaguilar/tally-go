//
//  OptionsShadow.swift
//  LAFD
//
//  Created by Anthony Picciano on 11/18/16.
//  Copyright © 2016 TALLYGO. All rights reserved.
//

import UIKit

class OptionsShadow: UIView {

    override func draw(_ rect: CGRect) {
        LAFDStyleKit.drawOptionsShadow(frame: bounds)
    }

}
