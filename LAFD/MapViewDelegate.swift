//
//  MapViewDelegate.swift
//  LAFD
//
//  Created by Anthony Picciano on 12/7/16.
//  Copyright © 2016 TALLYGO. All rights reserved.
//

import Foundation
import Mapbox
import TallyGoKit
import RxCocoa
import RxSwift

let MapViewDelegateDidTapIncident =
   NSNotification.Name("MapViewDelegateDidTapIncident")
let LAFDIncidentKey = "LAFDIncidentKey"

let MapViewDelegateDidSelectAnnotation =
   NSNotification.Name("MapViewDelegateDidSelectAnnotation")
let MapViewDelegateDidDeselectAnnotation =
   NSNotification.Name("MapViewDelegateDidDeselectAnnotation")

let LAFDAnnotationKey = "LAFDAnnotationKey"

class MapViewDelegate: NSObject, MGLMapViewDelegate {
   var selectedLocationType: String?
   
   let imageDownloader = ServiceLocator.imagesDownloader
   
   // Customize image for map marker here
   func mapView(_ mapView: MGLMapView, imageFor annotation: MGLAnnotation) -> MGLAnnotationImage? {
      
      var annotationImage: MGLAnnotationImage?
      
      if annotation is FireStationAnnotation {
         
         return MGLAnnotationImage(image: LAFDStyleKit.imageOfStationIcon, reuseIdentifier: LAFDStationReuseItentifier)
      }
      
      if annotation is UnitAnnotation {
         return MGLAnnotationImage(image: #imageLiteral(resourceName: "firetruck"), reuseIdentifier: LAFDTruckReuseIdentifier)
      }
      
      if let incidentAnnotation = annotation as? AlertAnnotation {
         return MGLAnnotationImage(image: #imageLiteral(resourceName: "ic_incident"), reuseIdentifier: incidentAnnotation.mapMarkerReuseKey())
      }
      
      if selectedLocationType == FacilityReuseIdentifier {
         return MGLAnnotationImage(image: LAFDStyleKit.imageOfFacility, reuseIdentifier: FacilityReuseIdentifier)
      }
      
      if selectedLocationType == LAFDStationReuseItentifier {
         return MGLAnnotationImage(image: LAFDStyleKit.imageOfStationIcon, reuseIdentifier: LAFDStationReuseItentifier)
      }
      
      guard let title: String = annotation.title! else {
         return nil
      }
      
      switch title {
      case LAFDStationReuseItentifier:
         annotationImage = MGLAnnotationImage(image: LAFDStyleKit.imageOfStationIcon, reuseIdentifier: LAFDStationReuseItentifier)
         
      case LAFDIncidentReuseIdentifier:
         annotationImage = MGLAnnotationImage(image: LAFDStyleKit.imageOfLAFDIncident, reuseIdentifier: LAFDIncidentReuseIdentifier)
         
      case LAFDAssignedIncidentReuseIdentifier:
         annotationImage = MGLAnnotationImage(image: #imageLiteral(resourceName: "ic_incident"), reuseIdentifier: LAFDAssignedIncidentReuseIdentifier)
         
      case FacilityReuseIdentifier:
         annotationImage = MGLAnnotationImage(image: LAFDStyleKit.imageOfFacility, reuseIdentifier: FacilityReuseIdentifier)
         
      case TrafficAccidentReuseIdentifier:
         annotationImage = MGLAnnotationImage(image: LAFDStyleKit.imageOfTrafficAccident, reuseIdentifier: TrafficAccidentReuseIdentifier)
         
      case RoadHazardReuseIdentifier:
         annotationImage = MGLAnnotationImage(image: LAFDStyleKit.imageOfRoadHazard, reuseIdentifier: RoadHazardReuseIdentifier)
         
      case DestinationReuseIdentifier:
         annotationImage = MGLAnnotationImage(image: #imageLiteral(resourceName: "ic_incident"), reuseIdentifier: LAFDIncidentReuseIdentifier)
         
      default:
         annotationImage = MGLAnnotationImage(image: LAFDStyleKit.imageOfMapMarker, reuseIdentifier: MapMarkerReuseIdentifier)
      }
      
      return annotationImage
   }
   
   func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
      return annotation is AlertAnnotation || annotation is UnitAnnotation
   }
   
   func mapView(_ mapView: MGLMapView, calloutViewFor annotation: MGLAnnotation) -> MGLCalloutView? {
      if let alertAnnotation = annotation as? AlertAnnotation {
         return alertAnnotation.calloutView()
      }
      
      if let unitAnnotation = annotation as? UnitAnnotation {
         return UnitCalloutView(representedObject: unitAnnotation)
      }
      
      return nil
   }
   
   func mapView(_ mapView: MGLMapView, didSelect annotation: MGLAnnotation) {
      NotificationCenter.default.post(
         name: MapViewDelegateDidSelectAnnotation,
         object: self,
         userInfo: [LAFDAnnotationKey: annotation]
      )
   }
   
   func mapView(_ mapView: MGLMapView, didDeselect annotation: MGLAnnotation) {
      NotificationCenter.default.post(
         name: MapViewDelegateDidDeselectAnnotation,
         object: self,
         userInfo: [LAFDAnnotationKey: annotation]
      )
   }
   
   func mapView(_ mapView: MGLMapView, tapOnCalloutFor annotation: MGLAnnotation) {
      if let alertAnnotation = annotation as? AlertAnnotation {
         NotificationCenter.default.post(
            name: MapViewDelegateDidTapIncident,
            object: self,
            userInfo: [LAFDIncidentKey : alertAnnotation.alert]
         )
      }
   }
   
   func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {
      log.info("Did finish loading style")
      // HACK: Reload the custom map layers when the style reloads
      top = 0
      bottom = 0
      left = 0
      right = 0
      processedHydrants.removeAll()
      processedPreplans.removeAll()
      self.mapView(mapView, regionDidChangeAnimated: false)
   }
   
   var disposable: Disposable?
   var processedHydrants: Set<Int> = Set()
   var processedPreplans: Set<Int> = Set()
   var top: Double = 0
   var bottom: Double = 0
   var left: Double = 0
   var right: Double = 0

   func mapView(_ mapView: MGLMapView, regionDidChangeAnimated animated: Bool) {
      let maxZoomLevel: Double = 14.8
      let bounds = mapView.visibleCoordinateBounds
      
      guard let style = mapView.style, mapView.zoomLevel >= maxZoomLevel else {
         print(bounds)
         return
      }
      
      let outerCoveredMargin = 0.02
      let outerVisibleMargin = outerCoveredMargin / 3
      
      let cleft = bounds.sw.longitude - outerVisibleMargin
      let cright = bounds.ne.longitude + outerVisibleMargin
      let ctop = bounds.sw.latitude - outerVisibleMargin
      let cbottom = bounds.ne.latitude + outerVisibleMargin
      
      if cright > right || cleft < left || ctop < top || cbottom > bottom {
         // needs to update the current layer
         top = ctop - outerCoveredMargin
         bottom = cbottom + outerCoveredMargin
         left = cleft - outerCoveredMargin
         right = cright + outerCoveredMargin
         
         self.disposable?.dispose()
         
         let layerService = ServiceLocator.layerService
         
         let hydrantsObservable: Observable<[Hydrant]> = layerService.getHydrants(
            left: left,
            right: right,
            top: top,
            bottom: bottom
         )
         
         let preplansObservable: Observable<[Preplan]> = layerService.getPreplans(
            left: left,
            right: right,
            top: top,
            bottom: bottom
         )
         
         self.disposable = Observable.combineLatest(
            hydrantsObservable,
            preplansObservable,
            resultSelector: { ($0, $1) })
            .observeOn(MainScheduler.instance)
               .subscribe(
                  onNext: { [weak self] (hydrants, preplans) in
                     guard let `self` = self else { return }
                     
                     // MARK: hydrants
                     let pendingHydrants = hydrants
                        .filter { !self.processedHydrants.contains($0.id) }
                     
                     pendingHydrants.forEach { self.processedHydrants.insert($0.id) }
                     
                     let hydrantPoints = pendingHydrants
                        .map { hydrant -> SymbolLayerAnnotation in
                           let point = SymbolLayerAnnotation(code: hydrant.code)
                           
                           point.coordinate = CLLocationCoordinate2D(
                              latitude: hydrant.latitude,
                              longitude: hydrant.longitude
                           )
                           
                           return point
                        }
                     
                     // MARK: Preplans
                     let pendingPreplans = preplans
                        .filter { !self.processedPreplans.contains($0.id) }
                     
                     pendingPreplans.forEach { self.processedPreplans.insert($0.id) }
                     
                     let preplanPoints = pendingPreplans
                        .filter { $0.latitude != nil && $0.longitude != nil }
                        .map { preplan -> SymbolLayerAnnotation in
                        let point = SymbolLayerAnnotation(code: preplan.code)
                        point.coordinate = CLLocationCoordinate2D(
                           latitude: preplan.latitude!,
                           longitude: preplan.longitude!
                        )
                        
                        return point
                     }
                     
                     self.add(annotations: hydrantPoints, in: style, withLevel: maxZoomLevel)
                     self.add(annotations: preplanPoints, in: style, withLevel: maxZoomLevel + 2)
               }, onError: { (error) in
                  print("Error retrieving map layers: \(error)")
                  self.left = 0
                  self.right = 0
                  self.top = 0
                  self.bottom = 0
               })
      }
   }
   
   func add(annotations: [SymbolLayerAnnotation], in style: MGLStyle, withLevel zoomLevel: Double) {
      let iconSize = CGSize(width: 40, height: 40)
      
      let categories = Dictionary(grouping: annotations, by: { $0.code })
      
      for (code, items) in categories {
         Observable.of(items).flatMap { [weak self] annotation -> Observable<(UIImage, [SymbolLayerAnnotation])> in
            guard let `self` = self else { return .empty() }
            
            return self.imageDownloader.getImage(forCode: code, size: iconSize).asObservable().withLatestFrom(Observable.just(annotation), resultSelector: { ($0, $1) })
            }.observeOn(MainScheduler.instance)
            .subscribe(onNext: { (icon, annotations) in
               style.setImage(icon, forName: code)
               let features = annotations.map { a -> MGLPointFeature in
                  let point = MGLPointFeature()
                  point.coordinate = a.coordinate
                  return point
               }
               
               let source = MGLShapeSource(identifier: UUID().uuidString, features: features, options: nil)
               let layer = MGLSymbolStyleLayer(identifier: UUID().uuidString, source: source)
               
               layer.iconImageName = MGLStyleValue(rawValue: code as NSString)
               layer.minimumZoomLevel = Float(zoomLevel)
               
               style.addSource(source)
               style.addLayer(layer)
            }).disposed(by: self.disposeBag)
      }

//      let categories = Dictionary(grouping: annotations, by: { $0.code })
//
//      for (code, items) in categories {
//         guard let codeImage = UIImage(named: code) else {
//            fatalError("Unable to find image for code: \(code)")
//         }
//
//         style.setImage(codeImage, forName: code)
//
//         let features = items.map { a -> MGLPointFeature in
//            let point = MGLPointFeature()
//            point.coordinate = a.coordinate
//            return point
//         }
//
//         let source = MGLShapeSource(identifier: UUID().uuidString, features: features, options: nil)
//         let layer = MGLSymbolStyleLayer(identifier: UUID().uuidString, source: source)
//
//         layer.iconImageName = MGLStyleValue(rawValue: code as NSString)
//         layer.minimumZoomLevel = Float(zoomLevel)
//
//         style.addSource(source)
//         style.addLayer(layer)
//      }
   }
   
   // Configure route line below
   func mapView(_ mapView: MGLMapView, lineWidthForPolylineAnnotation annotation: MGLPolyline) -> CGFloat {
      return 6.0
   }
   
   func mapView(_ mapView: MGLMapView, strokeColorForShapeAnnotation annotation: MGLShape) -> UIColor {
      return LAFDStyleKit.tGBlue
   }
   
   func mapView(_ mapView: MGLMapView, alphaForShapeAnnotation annotation: MGLShape) -> CGFloat {
      return 1.0
   }
}
