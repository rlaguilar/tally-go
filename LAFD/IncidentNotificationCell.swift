//
//  IncidentNotificationCell.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/8/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import UIKit
import RxSwift

class IncidentNotificationCell: UITableViewCell {
   static let identifier = "IncidentNotificationCell"
   @IBOutlet weak var incidentNumberLabel: UILabel!
   @IBOutlet weak var incidentAddress: UILabel!
   @IBOutlet weak var incidentCrossStreetLabel: UILabel!
   @IBOutlet weak var incidentLongDescriptionLabel: UILabel!
   @IBOutlet weak var dateLabel: UILabel!
   
   var reuseBag = DisposeBag()
   
   var item: AlertViewModel? {
      didSet {
         guard let item = self.item else { return }
         
         let alert = item.alert
         incidentNumberLabel.text = "Incident: \(alert.id)"
         dateLabel.text = alert.createdAt.formatToReadable()
         incidentLongDescriptionLabel.text = alert.dispatchType
         
         incidentAddress.text = alert.address
         incidentCrossStreetLabel.text = alert.city != nil ? "@ \(alert.city!)" : nil
         
         animate(animated: !item.isRead)
      }
   }
   
   func animate(animated: Bool) {
      let gray: CGFloat = CGFloat(51) / 255.0
      self.contentView.layer.removeAllAnimations()
      self.contentView.backgroundColor = UIColor(white: gray, alpha: 1)
      
      if animated {
         UIView.animate(withDuration: 0.2, delay: 0, options: [.repeat, .autoreverse, .allowUserInteraction], animations: {
            self.contentView.backgroundColor = UIColor.black
         }, completion: nil)
      }
   }
   
   override func awakeFromNib() {
      super.awakeFromNib()
      // Initialization code
      self.selectionStyle = .none
   }
   
   override func prepareForReuse() {
      super.prepareForReuse()
      reuseBag = DisposeBag()
      self.contentView.layer.removeAllAnimations()
   }
}
