//
//  BackButton.swift
//  LAFD
//
//  Created by Anthony Picciano on 11/21/16.
//  Copyright © 2016 TALLYGO. All rights reserved.
//

import UIKit

class BackButton: UIButton {
    
    override func draw(_ rect: CGRect) {
        LAFDStyleKit.drawBackButton()
    }

}
