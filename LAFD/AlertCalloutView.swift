//
//  AlertCalloutView.swift
//  LAFD
//
//  Created by Anthony Picciano on 1/25/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import UIKit
import Mapbox

class AlertCalloutView: UIView, MGLCalloutView {
   
   var representedObject: MGLAnnotation
   var leftAccessoryView: UIView = UIView() /* unused */
   var rightAccessoryView: UIView = UIView() /* unused */
   weak var delegate: MGLCalloutViewDelegate?
   
   private var isLeft = false
   
   required init(representedObject: MGLAnnotation) {
      self.representedObject = representedObject
      
      guard let annotation = representedObject as? AlertAnnotation else {
         fatalError()
      }
      
      let title = annotation.title ?? "Incident"
      let subtitle = annotation.subtitle ?? "Address missing"
      let crossStreet = annotation.crossStreet ?? "Cross street"
      
      let body: String
      
      if annotation.addressId != nil {
         body = "Loading story...\n\n\n"
      }
      else {
         body = "Missing story"
      }
      
      let defaultFontSize: CGFloat = 14
      
      let str = NSMutableAttributedString(string: "\(title)\n\(subtitle)\n\(crossStreet)\n\n\(body)")
      
      str.setAttributes(
         [NSAttributedStringKey.font: UIFont.systemFont(ofSize: defaultFontSize + 6, weight: UIFont.Weight.black)],
         range: (str.string as NSString).range(of: subtitle))
      
      str.setAttributes(
         [NSAttributedStringKey.foregroundColor: UIColor.lightGray],
         range: (str.string as NSString).range(of: crossStreet))
      
      let textContainer = UITextView()
      textContainer.attributedText = str
      textContainer.isEditable = false
      textContainer.isScrollEnabled = true
      let maxHeight: CGFloat = 250
      let expectedSize = textContainer.sizeThatFits(CGSize(width: 280, height: maxHeight))
      
      let padding: CGFloat = 4
      
      super.init(frame: CGRect(x: 0, y: 0, width: expectedSize.width + 2 * padding, height: min(maxHeight, expectedSize.height) + 2 * padding))
      
      let contentView = UIView()
      
      self.addSubview(contentView)
      contentView.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
      
      contentView.addSubview(textContainer)
      textContainer.frame = contentView.bounds.insetBy(dx: padding, dy: padding)
      
      self.backgroundColor = .clear
      contentView.backgroundColor = .white
      let layer = contentView.layer
      layer.masksToBounds = true
      layer.cornerRadius = 4
      self.layer.shadowOffset = .zero
      self.layer.shadowOpacity = 0.5
      self.layer.shadowRadius = 3;
      
      if let addressId = annotation.addressId {
         let alertService = ServiceLocator.alertService
         alertService.getStory(alertId: addressId)
            .map { $0.story }
            .subscribe(onNext: { story in
               let str = NSMutableAttributedString(string: "\(title)\n\(subtitle)\n\(crossStreet)\n\n\(story)")
               
               str.setAttributes(
                  [NSAttributedStringKey.font: UIFont.systemFont(ofSize: defaultFontSize + 6, weight: UIFont.Weight.black)],
                  range: (str.string as NSString).range(of: subtitle))
               
               str.setAttributes(
                  [NSAttributedStringKey.foregroundColor: UIColor.lightGray],
                  range: (str.string as NSString).range(of: crossStreet))
               
               textContainer.attributedText = str
            }).disposed(by: disposeBag)
      }
      
   }

   required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   func presentCallout(from rect: CGRect, in view: UIView, constrainedTo constrainedView: UIView, animated: Bool) {
      guard representedObject is AlertAnnotation else {
         return
      }
      
      view.addSubview(self)
      
      isLeft = rect.midX > view.bounds.midX
      
      if isCalloutTappable() {
         // Handle taps and eventually try to send them to the delegate (usually the map view)
         let tap = UITapGestureRecognizer(target: self, action: #selector(calloutTapped))
         self.addGestureRecognizer(tap)
      } else {
         // Disable tapping and highlighting
         isUserInteractionEnabled = false
      }
      
      let frameWidth = bounds.size.width
      let frameHeight = bounds.size.height
      
      let frameOriginX: CGFloat
      let frameOriginY: CGFloat
      let minY: CGFloat = 112
      let minX: CGFloat = 8
      let padding: CGFloat = 8
      
      if rect.origin.x + rect.width + frameWidth < constrainedView.bounds.width {
         // put the view to the right of the marker
         frameOriginX = rect.origin.x + rect.width
         frameOriginY = max(minY, min(rect.origin.y + rect.height / 2 - frameHeight / 2, constrainedView.bounds.height - frameHeight - padding))
      }
      else if rect.origin.x - frameWidth > 0 {
         // put the view to the left
         frameOriginX = rect.origin.x - frameWidth
         frameOriginY = max(minY, min(rect.origin.y + rect.height / 2 - frameHeight / 2, constrainedView.bounds.height - frameHeight - padding))
      }
      else if rect.origin.y - frameHeight > minY {
         // put the view to the top
         frameOriginY = rect.origin.y - frameHeight
         frameOriginX = max(minX, min(constrainedView.bounds.width - frameWidth - padding, rect.origin.x + rect.width / 2 - frameWidth / 2))
      }
      else {
         // put the view to the bottom
         frameOriginY = rect.origin.y + rect.height
         frameOriginX = max(minX, min(constrainedView.bounds.width - frameWidth - padding, rect.origin.x + rect.width / 2 - frameWidth / 2))
      }

      frame = CGRect(x: frameOriginX, y: frameOriginY, width: frameWidth, height: frameHeight)
      
      if animated {
         alpha = 0
         
         UIView.animate(withDuration: 0.2) {
            self.alpha = 1
         }
      }
   }
   
   func dismissCallout(animated: Bool) {
      if superview != nil {
         if animated {
            UIView.animate(withDuration: 0.2, animations: {
               self.alpha = 0
            }) { (completed) in
               self.removeFromSuperview()
            }
         } else {
            removeFromSuperview()
         }
      }
   }
   
   // MARK: - Callout interaction handlers
   
   func isCalloutTappable() -> Bool {
      if let delegate = delegate {
         if delegate.responds(to: #selector(MGLCalloutViewDelegate.calloutViewShouldHighlight(_:))) {
            return delegate.calloutViewShouldHighlight!(self)
         }
      }
      return false
   }
   
   @objc func calloutTapped() {
      if isCalloutTappable() && delegate!.responds(to: #selector(MGLCalloutViewDelegate.calloutViewTapped(_:))) {
         delegate!.calloutViewTapped!(self)
      }
   }
}
