//
//  DrivingSimulatorViewController.swift
//  LAFD
//
//  Created by Anthony Picciano on 11/21/16.
//  Copyright © 2016 TALLYGO. All rights reserved.
//

import UIKit
import TallyGoKit
import CoreLocation

class DrivingSimulatorViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, AdjustableTableHeight {

    @IBOutlet weak var tableView: UITableView!
    
    let titles = [ "Enable Driving Simulator", "Highway Speed", "City Street Speed", "Starting Location" ]
    
    //MARK: - Table view data source and delegate methods
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        adjustTableHeight()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let accessory = UISwitch()
            accessory.tag = indexPath.row
            accessory.isOn = TallyGoKit.simulatedCoordinate != nil
            accessory.addTarget(self, action: #selector(switchChanged), for: .valueChanged)
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Driving Simulator Cell", for: indexPath)
            cell.textLabel?.text = titles[indexPath.row]
            cell.accessoryView = accessory
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Value Cell", for: indexPath) as! ValueTableViewCell
            cell.title = titles[indexPath.row]
            cell.value = value(for: indexPath.row)
            cell.accessoryType = .disclosureIndicator
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = .zero
        cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case 0:
            let cell = tableView.cellForRow(at: indexPath)
            if let accessory = cell?.accessoryView as? UISwitch {
                accessory.setOn(!accessory.isOn, animated: true)
                switchChanged(sender: accessory)
            }
        case 1:
            selectHighwaySpeed()
        case 2:
            selectCitySpeed()
        case 3:
            selectStartingLocation()
        default:
            break
        }
    }
    
    //MARK: - Internal methods
    
   @objc func switchChanged(sender: Any?) {
        if let accessory = sender as? UISwitch, accessory.isOn {
            TallyGoKit.simulatedCoordinate = StartingLocation.savedStartingLocation.coordinate
            StartingLocation.savedDrivingSimulatorEnabled = true
            StartingLocation.announceSimulator()
        } else {
            TallyGoKit.simulatedCoordinate = nil
            StartingLocation.savedDrivingSimulatorEnabled = false
        }
    }
    
    //MARK: - Private methods
    
    private func value(for row: Int) -> String? {
        switch row {
        case 1:
            return String(format: "%i MPH", TallyGoKit.simulatedHighwaySpeed.mphValue)
        case 2:
            return String(format: "%i MPH", TallyGoKit.simulatedCitySpeed.mphValue)
        case 3:
            return StartingLocation.savedStartingLocation.name
        default:
            return nil
        }
    }
    
    private func selectHighwaySpeed() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        
        let superHighSpeed = UIAlertAction(title: "135 MPH – Welcome to LA!", style: .default) { action in
            TallyGoKit.simulatedHighwaySpeed = CLLocationSpeed(mph: 135)
            self.tableView.reloadData()
        }
        
        let highSpeed = UIAlertAction(title: "100 MPH – NITRO!", style: .default) { action in
            TallyGoKit.simulatedHighwaySpeed = CLLocationSpeed(mph: 100)
            self.tableView.reloadData()
        }
        
        let lowSpeed = UIAlertAction(title: "65 MPH – Normal", style: .default) { action in
            TallyGoKit.simulatedHighwaySpeed = CLLocationSpeed(mph: 65)
            self.tableView.reloadData()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        alert.addAction(superHighSpeed)
        alert.addAction(highSpeed)
        alert.addAction(lowSpeed)
        alert.addAction(cancelAction)
        
        present(alert, animated: true)
    }
    
    private func selectCitySpeed() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        
        let ludacrisSpeed = UIAlertAction(title: "120 MPH – OMG TOM!?!", style: .default) { action in
            TallyGoKit.simulatedCitySpeed = CLLocationSpeed(mph: 120)
            self.tableView.reloadData()
        }
        
        let superHighSpeed = UIAlertAction(title: "90 MPH – HOLD ON!", style: .default) { action in
            TallyGoKit.simulatedCitySpeed = CLLocationSpeed(mph: 90)
            self.tableView.reloadData()
        }
        
        let highSpeed = UIAlertAction(title: "60 MPH – PUNCH IT!", style: .default) { action in
            TallyGoKit.simulatedCitySpeed = CLLocationSpeed(mph: 60)
            self.tableView.reloadData()
        }
        
        let lowSpeed = UIAlertAction(title: "30 MPH – Normal", style: .default) { action in
            TallyGoKit.simulatedCitySpeed = CLLocationSpeed(mph: 30)
            self.tableView.reloadData()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        alert.addAction(ludacrisSpeed)
        alert.addAction(superHighSpeed)
        alert.addAction(highSpeed)
        alert.addAction(lowSpeed)
        alert.addAction(cancelAction)
        
        present(alert, animated: true)
    }
    
    private func selectStartingLocation() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        
        for location in StartingLocation.locations {
            let action = UIAlertAction(title: location.name, style: .default) { action in
                StartingLocation.savedStartingLocation = location
                self.tableView.reloadData()
            }
            alert.addAction(action)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        alert.addAction(cancelAction)
        
        present(alert, animated: true)
    }

}
