//
//  AutocompleteView.swift
//  First Due Mobile
//
//  Created by Reynaldo Aguilar on 11/22/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import UIKit
import RxSwift

class AutocompleteView: UIView {
   let tableView = UITableView()
   let searchBar = UISearchBar()
   
   private var isSuggestionViewHidden = false
   
   let suggestions: Variable<[AutocompleteItem]> = Variable([])
   
   private lazy var emtpyResultsView: UIView = {
      let label = UILabel()
      label.text = "   No results"
      label.textColor = .black
      label.backgroundColor = .white
      label.sizeToFit()
      return label
   }()
   
   let dataSource = AutocompleteDataSource()
   
   lazy var selectedItem = self.dataSource.selectedItem
      .do(onNext: { [unowned self] _ in
//         self.hideSuggestionsView(hide: true, animated: true)
         if let index = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: index, animated: true)
         }
         
         self.searchBar.endEditing(true)
      })
   
   func hideSuggestionsView(hide: Bool, animated: Bool) {
      isSuggestionViewHidden = hide
      
      let block = { [unowned self] (hide: Bool) in
         self.tableView.alpha = hide ? 0 : 1
      }
      
      if animated {
         UIView.animate(withDuration: 0.2, animations: {
            block(hide)
         })
      }
      else {
         block(hide)
      }
   }
   
   func update(suggestions: [AutocompleteItem]) {
      self.suggestions.value = suggestions
      self.dataSource.items = suggestions
      self.tableView.reloadData()
   }
   
   override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
      if searchBar.frame.contains(point) {
         return true
      }
      
      if !isSuggestionViewHidden {
         return self.tableView.frame.contains(point)
      }
      else {
         return false
      }
   }
   
   func initialize() {
      self.addSubview(tableView)
      self.addSubview(searchBar)
      
      // positioning items
      let searchFrame = CGRect(
         origin: .zero,
         size: CGSize(width: bounds.width, height: 44)
      )
      
      searchBar.frame = searchFrame
      searchBar.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
      
      let tableFrame = CGRect(
         x: 0,
         y: searchFrame.height,
         width: bounds.width,
         height: bounds.height - searchFrame.height
      )
      
      tableView.frame = tableFrame
      tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      
      // customizating search bar
      searchBar.barStyle = .black
      searchBar.searchBarStyle = .minimal
      searchBar.delegate = self
   
      // customizing table view
      let nib = UINib(nibName: "AutocompleteCell", bundle: .main)
      tableView.register(nib, forCellReuseIdentifier: "identifier")
      tableView.rowHeight = UITableViewAutomaticDimension
      tableView.estimatedRowHeight = 44
      tableView.dataSource = dataSource
      tableView.delegate = dataSource
      
      let nilView = UIView()
      
      Observable.combineLatest(
         searchBar.rx.text.asObservable(),
         self.suggestions.asObservable()) { (query, suggestions) -> Bool in
            return query != "" && suggestions.isEmpty
         }.subscribe(onNext: { [unowned self] showFooter in
            if showFooter {
               self.tableView.tableFooterView = self.emtpyResultsView
            }
            else {
               self.tableView.tableFooterView = nilView
            }
            
            self.tableView.backgroundColor = .clear

         }).disposed(by: disposeBag)
      
      self.hideSuggestionsView(hide: true, animated: false)
   }
   
   override init(frame: CGRect) {
      super.init(frame: frame)
      self.initialize()
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      self.initialize()
   }
}

extension AutocompleteView: UISearchBarDelegate {
   func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
      self.hideSuggestionsView(hide: true, animated: true)
      searchBar.setShowsCancelButton(false, animated: true)
   }
   
   func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
      self.hideSuggestionsView(hide: searchBar.text == "", animated: true)
      searchBar.setShowsCancelButton(true, animated: true)
   }
   
   func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
      self.hideSuggestionsView(hide: searchText.isEmpty, animated: true)
   }
   
   func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
      searchBar.endEditing(true)
   }
}

class AutocompleteDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
   let _selectedItem = PublishSubject<AutocompleteItem>()
   lazy var selectedItem = self._selectedItem.asObservable()
   
   var items: [AutocompleteItem] = []
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return items.count
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "identifier", for: indexPath) as! AutocompleteCell
      cell.item = self.items[indexPath.row].value
      return cell
   }
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      _selectedItem.onNext(items[indexPath.row])
   }
}

struct AutocompleteItem {
   let value: String
   let key: String
}
