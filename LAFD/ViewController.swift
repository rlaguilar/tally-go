//
//  ViewController.swift
//  LAFD
//
//  Created by Anthony Picciano on 11/2/16.
//  Copyright © 2016 TALLYGO. All rights reserved.
//

import UIKit
import Mapbox
import TallyGoKit
import RxSwift
import RxCocoa

let LAFDIncidentReuseIdentifier = "LAFD Incident"
let LAFDAssignedIncidentReuseIdentifier = "LAFD Assigned Incident"
let LAFDStationReuseItentifier = "LAFD Station"
let LAFDTruckReuseIdentifier = "LAFD Truck"
let FacilityReuseIdentifier = "Facility"
let TrafficAccidentReuseIdentifier = "Traffic Accident"
let RoadHazardReuseIdentifier = "Road Hazard"
let MapMarkerReuseIdentifier = "Map Marker"
let DestinationReuseIdentifier = "destination"

class ViewController: UIViewController {
   let updatePositionTimeInterval: TimeInterval = 10
   let retrievePositionsTimeInterval: TimeInterval = 10
   
   @IBOutlet weak var searchViewWrapper: UIView!
   
   var mapView: MGLMapView!
   var task: URLSessionDataTask?
   var manualLocationSelectionInProgress = false
   
   weak var navViewController: TGGuidanceViewController?
   
   // Retain this instance
   let mapViewDelegate = MapViewDelegate()
   
   let navigationMapViewDelegate = MapViewDelegate()
   
   var selectedAddress: TGAddress?
   
   var selectedLocation: CLLocationCoordinate2D? {
      didSet {
         task?.cancel()
         
         if selectedLocation != nil {
            loadRoute()
         }
      }
   }
   
   var route: TGRoute?
   
   weak var selectedAnnotation: AlertAnnotation?
   
   var searchController: CustomSearchController!
   var resultsController: SearchResultsViewController!
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      insertMapView()
      
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(simulatedCoordinateDidChangeHandler),
         name: TallyGoKitSimulatedCoordinateDidChangeNotification,
         object: nil
      )
      
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(didSelectIncidentHandler),
         name: MapViewDelegateDidTapIncident,
         object: mapViewDelegate
      )
      
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(didSelectAnnontationHandler(notification:)),
         name: MapViewDelegateDidSelectAnnotation,
         object: mapViewDelegate
      )
      
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(didDeselectAnnotationHandler(notification:)),
         name: MapViewDelegateDidDeselectAnnotation,
         object: mapViewDelegate
      )
      
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(didSelectIncidentNotificationHandler(notification:)),
         name: NotificationListDidSelectItem,
         object: nil
      )
      
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(alertItemsChanged(notification:)),
         name: AlertItemsChanged,
         object: nil
      )
      
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(self.showSizeupHandler(_:)),
         name: ShowSizeupNotification,
         object: nil
      )
      
      NotificationCenter.default.addObserver(
         self,
         selector: #selector(self.rollNotificationHandler(_:)),
         name: RollToAlertNotification,
         object: nil
      )
      
      TGConfiguration.get { response in
         log.info("NavigationRootURL: \(response.tallyGoNavigationRootURL as Any)")
         log.info("VoxRootURL: \(response.tallyGoVoxRootURL as Any)")
         log.info("FindRootURL: \(response.tallyGoFindRootURL as Any)")
      }
      
//      handleSearch()

      resultsController = SearchResultsViewController.create()
      searchController = CustomSearchController(searchResultsController: resultsController)
      searchController.searchBar.sizeToFit()
      
      let searchBar = searchController.searchBar
      searchBar.backgroundColor = .clear
      searchBar.searchBarStyle = .minimal
      searchBar.barStyle = .black
      searchViewWrapper.addSubview(searchBar)

      searchBar.frame = searchViewWrapper.bounds
      searchBar.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
      self.handleSearchBar()
      self.handleSearchSelection()
      self.handleUnitsTracking()
   }
   
   func handleUnitsTracking() {
      let service = ServiceLocator.userService
      
      Observable<Int>.timer(
         updatePositionTimeInterval,
         period: updatePositionTimeInterval,
         scheduler: MainScheduler.instance
         ).map { [unowned self] _ in self.mapView.userLocation?.coordinate }
         .filter { $0 != nil }
         .map { $0! }
         .flatMapLatest({ (coordinates ) -> Observable<Void> in
            service.updateLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
         }).subscribe(onNext: {
            print("updated ok")
         }).disposed(by: disposeBag)
      
      Observable<Int>.timer(
         retrievePositionsTimeInterval,
         period: retrievePositionsTimeInterval,
         scheduler: MainScheduler.instance
         ).flatMapLatest { _ in service.getUnitsLocation() }
         // TODO: this is a test code. It should be removed
//         .map { units -> [Unit] in
//            var newUnits = units
//
//            if let unit = units.first {
//               let u1 = Unit(name: "Clone 1", type: "awesome", latitude: unit.latitude - 0.01, longitude: unit.longitude - 0.01, updatedAt: Date())
//               let u2 = Unit(name: "Clone 2", type: "awesome", latitude: unit.latitude + 0.01, longitude: unit.longitude + 0.01, updatedAt: Date())
//
//               newUnits.append(u1)
//               newUnits.append(u2)
//            }
//
//            return newUnits
//         }
         .observeOn(MainScheduler.instance)
         .subscribe(onNext: { units in
            print("Retrieved units: \(units)")
            let newAnnotations = units.map(UnitAnnotation.init(unit:))
            let currentAnnotations = self.mapView.annotations?.filter { $0 is UnitAnnotation } ?? []
            self.mapView.removeAnnotations(currentAnnotations)
            self.mapView.addAnnotations(newAnnotations)
         }).disposed(by: disposeBag)
   }
   
   func handleSearchBar() {
      self.searchController.updateResults
         .flatMapLatest { [unowned self] query -> Observable<[TGSuggestion]> in
            guard let coordinates = self.mapView.userLocation?.coordinate else {
               return .empty()
            }

            let suggestRequest = TGSuggestRequest(text: query, location: coordinates)

            return Observable.create({ (observer) -> Disposable in
               let task = TGFind.suggest(request: suggestRequest, completionHandler: { (response) in
                  if !response.isCancelled() {
                     observer.onNext(response.suggestions ?? [])
                     observer.onCompleted()
                  }
               })

               return Disposables.create {
                  task?.cancel()
               }
            })
         }.subscribe(onNext: { [unowned self] suggestions in
            log.info(suggestions.map { $0.json })
   
            let items = suggestions.map {
               AutocompleteItem(
                  value: $0.text ?? "",
                  key: $0.suggestionKey ?? ""
               )
            }

            self.resultsController.items = items
         }).disposed(by: disposeBag)
   }

   func handleSearchSelection() {
      self.resultsController.selectedItem
         .do(onNext: { [weak self] _ in
            self?.searchController.isActive = false
         })
         .flatMapLatest({ [unowned self] (item) -> Observable<[TGSearchResult]> in
         guard let coordinates = self.mapView.userLocation?.coordinate else {
            return .empty()
         }

         let searchRequest = TGSearchRequest(singleLine: item.value, location: coordinates, searchExtent: nil, suggestionKey: item.key)

         return Observable.create({ (observer) -> Disposable in
            let task = TGFind.search(request: searchRequest, completionHandler: { (response) in
               guard !response.isCancelled() else { return }

               observer.onNext(response.results)
               observer.onCompleted()
            })

            return Disposables.create {
               task?.cancel()
            }
         })
      }).subscribe(onNext: { [unowned self] results in
         guard let result = results.first(where: { $0.location != nil }) else { return }

         guard let coordinates = result.location else {
            fatalError()
         }

         self.mapView.setCenter(coordinates, zoomLevel: 17, animated: true)
      }).disposed(by: disposeBag)
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      log.info("viewWillAppear")
      
      if manualLocationSelectionInProgress {
         return
      }
      
      if route != nil || selectedAddress != nil || selectedLocation != nil {
         cancelAction(self)
      }
   }
   
   override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)
      
      if manualLocationSelectionInProgress || selectedAddress != nil || selectedLocation != nil || route != nil {
         manualLocationSelectionInProgress = false
      } else {
         // TODO: Check for incidents here
         //         checkForIncidentAction(self)
         //         showIncidents()
         // TODO: fix call here
         show(alerts: self.currentAlerts, removingPrevious: true)
      }
   }
   
   func displayRoute() {
      let vc = TGRoutePreviewViewController.makeFromStoryboard()
      vc.destinationAddress = self.selectedAddress?.displayAddress
      vc.segment = self.route?.segments.first
      
      if let origin = self.mapView.userLocation?.coordinate {
         vc.origin = origin
      }
      
      vc.onTallyGo = {
         self.navigationController?.popViewController(animated: false)
         let viewController = TGGuidanceViewController.makeFromStoryboard()
         viewController.destination = self.selectedLocation
         viewController.segment = self.route?.segments.first
         viewController.showsOriginIcon = false
         viewController.mapview.zoomLevel = 16
         viewController.mapview.tintColor = LAFDStyleKit.tGGreen
         viewController.mapview.delegate = self.navigationMapViewDelegate
         viewController.destinationAddress = self.selectedAddress?.displayAddress
         viewController.commencementSpeech = "Let's roll."
         viewController.arrivalSpeech = "You have arrived."
         
         if let origin = self.mapView.userLocation?.coordinate {
            viewController.origin = origin
            viewController.mapview.centerCoordinate = origin
         }
         
         self.navViewController = viewController
         self.navigationController?.pushViewController(viewController, animated: false)
      }
   
      self.show(vc, sender: self)
      self.navigationController?.setNavigationBarHidden(false, animated: true)
   }
   
   @objc func simulatedCoordinateDidChangeHandler(notification: Notification) {
      log.info("Changed coordinates")
      insertMapView()
   }
   
   @objc func didSelectIncidentHandler(notification: Notification) {
      manualLocationSelectionInProgress = true
      if let alert = notification.userInfo?[LAFDIncidentKey] as? Alert {
         self.updateSelectedAlert(alert: alert)
      }
   }
   
   private func updateSelectedAlert(alert: Alert) {
      selectedAddress = alert.tallyGoAddress
      mapViewDelegate.selectedLocationType = LAFDAssignedIncidentReuseIdentifier
      selectedLocation = alert.coordinates
   }
   
   @objc func didSelectAnnontationHandler(notification: Notification) {
      if let annotation = notification.userInfo?[LAFDAnnotationKey] as? AlertAnnotation {
         self.selectedAnnotation = annotation
      }
   }
   
   @objc func didDeselectAnnotationHandler(notification: Notification) {
//      self.selectedAnnotation = nil
   }
   
   @objc func didSelectIncidentNotificationHandler(notification: Notification) {
      guard let alert = notification.userInfo?[IncidentNotificationKey] as? Alert,
            let coordinates = alert.coordinates else {
         return
      }
      
      self.mapView.setCenter(
         coordinates,
         zoomLevel: max(self.mapView.zoomLevel, 17),
         direction: self.mapView.direction,
         animated: true,
         completionHandler: nil
      )
   }
   
   var currentAlerts: [Alert] = []
   
   @objc func alertItemsChanged(notification: Notification) {
      guard let items = notification.userInfo?[AlertItemsKey] as? [Alert] else {
         return
      }
      
      currentAlerts.append(contentsOf: items)
      
      self.show(alerts: currentAlerts, removingPrevious: false)
   }
   
   func show(alerts: [Alert], removingPrevious shouldRemove: Bool) {
      // Ensure that we do not already have a destination selected.
      // https://www.pivotaltracker.com/story/show/138660471
      if self.selectedLocation != nil || self.selectedAddress != nil {
         return
      }

      if shouldRemove {
         self.mapView.removeAnnotations(
            self.mapView.annotations?.filter { $0 is AlertAnnotation } ?? []
         )
      }

      let annotations = alerts.filter { $0.coordinates != nil }
         .map { try! AlertAnnotation(alert: $0) }

      self.mapView.addAnnotations(annotations)
   }
   
   func insertMapView() {
      log.debug("Inserting new map view here")
      mapView?.delegate = nil
      mapView?.removeFromSuperview()
   
      mapView = createMapView()
      view.insertSubview(mapView, at: 0)
      mapView.frame = view.bounds
      mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
   }
   
   func createMapView() -> MGLMapView {
      let newMapView = TallyGoKit.createMapView()
      
      newMapView.tintColor = LAFDStyleKit.tGGreen
      newMapView.logoView.isHidden = true
      newMapView.attributionButton.isHidden = true
      newMapView.latitude = 34.0522 // set initial point to avoid starting zoom from Pacific Ocean
      newMapView.longitude = -118.2437
      newMapView.zoomLevel = 12
      newMapView.showsUserLocation = true
      newMapView.userTrackingMode = .follow
   
      newMapView.delegate = mapViewDelegate
      
      newMapView.contentInset = UIEdgeInsets(top: 110, left: 0, bottom: 0, right: 0)
      
      newMapView.updateConstraints()

      return newMapView
   }
   
   @objc func showSizeupHandler(_ notification: Notification) {
      guard let alert = notification.userInfo?[AlertNotificationKey] as? Alert else {
         return
      }
      
      showSizeup(for: alert)
   }

   func showSizeup(for alert: Alert) {
      let viewController = WebViewController.instantiate(from: .Main)
      viewController.initialUrl = alert.url
      self.show(viewController, sender: self)
   }
   
   @objc func rollNotificationHandler(_ notification: Notification) {
      guard let alert = notification.userInfo?[AlertNotificationKey] as? Alert else {
         return
      }
      
      self.updateSelectedAlert(alert: alert)
   }
   
   @IBAction func cancelAction(_ sender: Any) {
      selectedAddress = nil
      selectedLocation = nil
      route = nil
      navViewController?.mapview.delegate = nil
//      mapView.userTrackingMode = .follow
      
      // TODO: fix call here
      self.show(alerts: currentAlerts, removingPrevious: true)
   }
   
   var currentLoadingId: String? = nil
   
   func loadRoute() {
      guard let selectedLocation = selectedLocation else {
         return
      }
      
      guard let currentLocation = mapView.userLocation?.coordinate else {
         return
      }
      
      route = nil
      
      if let id = currentLoadingId {
         MessagesHandler.hideProgress(with: id)
      }
      
      currentLoadingId = MessagesHandler.showProgress(
         message: "Computing best route..."
      )
      
      let coords = [currentLocation, selectedLocation]
      let time = Date() // now
      let speed = TallyGoKit.currentLocation?.speed
      let course = TallyGoKit.currentLocation?.course
      let request = TGRouteRequest(coords: coords, time: time, requestType: .DepartureTime, speed: speed, course: course)
      
      task = TGNavigation.route(request: request) { (response) in
         if let id = self.currentLoadingId {
            MessagesHandler.hideProgress(with: id)
         }
         
         self.currentLoadingId = nil
         
         if let error = response.error {
            log.error("Error Getting Route: \(error)")
            
            if (error as NSError).code != -999 {
               MessagesHandler.showError(
                  message: error.localizedDescription,
                  title: "Error Getting Route"
               )
            }
         }
         else {
            self.route = response.route
            //         self.mapView.setCenter(selectedLocation, animated: true)
            self.displayRoute()
         }
      }
   }
   
   func show(error: Error, title: String = "Error") {
      show(message: error.localizedDescription, title: title)
   }
   
   func show(message: String, title: String) {
      let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
      present(alert, animated: true, completion: nil)
   }
   
}
