//
//  AutocompleteCell.swift
//  First Due Mobile
//
//  Created by Reynaldo Aguilar on 11/22/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import UIKit

class AutocompleteCell: UITableViewCell {
   @IBOutlet weak var addressLabel: UILabel!
   
   var item: String? {
      didSet {
         addressLabel.text = item
      }
   }
   
   override func awakeFromNib() {
      super.awakeFromNib()
      // Initialization code
   }
   
   override func setSelected(_ selected: Bool, animated: Bool) {
      super.setSelected(selected, animated: animated)
      
      // Configure the view for the selected state
   }
   
}
