//
//  UISearchController+.swift
//  First Due Mobile
//
//  Created by Reynaldo Aguilar on 11/24/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift

class CustomSearchController: UISearchController, UISearchResultsUpdating {
   private let _updateResults = PublishSubject<String>()
   lazy var updateResults = self._updateResults.asObservable()
   
   func updateSearchResults(for searchController: UISearchController) {
      self._updateResults.onNext(searchController.searchBar.text ?? "")
   }
   
   func initialize() {
      self.searchResultsUpdater = self
   }
   
   override init(searchResultsController: UIViewController?) {
      super.init(searchResultsController: searchResultsController)
      self.initialize()
   }
   
   override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
      super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
      self.initialize()
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      self.initialize()
   }
}
