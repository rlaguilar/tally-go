//
//  UIApplication+Screenshot.swift
//  LAFD
//
//  Created by Anthony Picciano on 11/18/16.
//  Copyright © 2016 TALLYGO. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    
   static func generateScreenshot(fromView: UIView? = UIApplication.shared.keyWindow) -> UIImage? {
        var image: UIImage?
        if let view = fromView {
            UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, UIScreen.main.scale)
            view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
            image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
        }
        
        return image
    }
    
}
