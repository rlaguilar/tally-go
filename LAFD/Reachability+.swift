//
//  Reachability+.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 9/13/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import Reachability
import RxSwift

extension Reachability {
   enum Errors: Error {
      case unavailable
   }
}

extension Reactive where Base: Reachability {
   static var reachable: Observable<Bool> {
      return Observable.create { observer in
         
         let reachability = Reachability()
         
         if let reachability = reachability {
            observer.onNext(reachability.isReachable)
            reachability.whenReachable = { _ in observer.onNext(true) }
            reachability.whenUnreachable = { _ in observer.onNext(false) }
            try? reachability.startNotifier()
         } else {
            observer.onError(Reachability.Errors.unavailable)
         }
         
         return Disposables.create {
            reachability?.stopNotifier()
         }
      }
   }
}
