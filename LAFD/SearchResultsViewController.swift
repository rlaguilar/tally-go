//
//  SearchResultsViewController
//  First Due Mobile
//
//  Created by Reynaldo Aguilar on 11/24/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SearchResultsViewController: UITableViewController {
   static func create() -> SearchResultsViewController {
      let vc = SearchResultsViewController(style: .plain)
      
      return vc
   }
   
   var items: [AutocompleteItem] = [] {
      didSet {
         self.tableView.reloadData()
      }
   }
   
   private let _selectedItem = PublishSubject<AutocompleteItem>()
   lazy var selectedItem = self._selectedItem.asObservable()
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      let nib = UINib(nibName: "AutocompleteCell", bundle: .main)
      tableView.register(nib, forCellReuseIdentifier: "identifier")
      tableView.rowHeight = UITableViewAutomaticDimension
      tableView.estimatedRowHeight = 44
      
      // Uncomment the following line to preserve selection between presentations
      // self.clearsSelectionOnViewWillAppear = false
      
      // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
      // self.navigationItem.rightBarButtonItem = self.editButtonItem
   }
   
   override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
   }
   
   // MARK: - Table view data source
   
   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return items.count
   }
   
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "identifier", for: indexPath) as! AutocompleteCell
      cell.item = self.items[indexPath.row].value
      return cell
   }
   
   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      let item = self.items[indexPath.row]
      self._selectedItem.onNext(item)
   }
   
   /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    // Return false if you do not want the specified item to be editable.
    return true
    }
    */
   
   /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
    // Delete the row from the data source
    tableView.deleteRows(at: [indexPath], with: .fade)
    } else if editingStyle == .insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
   
   /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
    
    }
    */
   
   /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
    // Return false if you do not want the item to be re-orderable.
    return true
    }
    */
   
   /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
   
}
