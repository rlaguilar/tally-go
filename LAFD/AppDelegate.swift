//
//  AppDelegate.swift
//  LAFD
//
//  Created by Anthony Picciano on 11/2/16.
//  Copyright © 2016 TALLYGO. All rights reserved.
//

import UIKit
import TallyGoKit
import Mapbox
import SwiftyBeaver
import UserNotifications
import Foundation
import Firebase
import RxCocoa
import RxSwift
import NVActivityIndicatorView

let log = SwiftyBeaver.self

let RemoteNotificationArrived = Notification.Name("RemoteNotificationArrived")

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
   
   static var instance: AppDelegate {
      return UIApplication.shared.delegate as! AppDelegate
   }
   
   var showLeftPanel: Bool = false
   var window: UIWindow?
   var reachability: Reachability? = Reachability()
   
   // Access token for TallyGo SDK
   let ACCESS_TOKEN = "c0bc925781bd7ed75da07e878a685de7"
   
   func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
      BuddyBuildSDK.setup()
      FirebaseConfiguration.shared.setLoggerLevel(.min)
      FirebaseApp.configure()
      
      self.handleNetworkConnection()
      self.setUserAgent()
      self.registerForPushNotifications()
      self.startListeningMessages()
      // Configure logging
      let console = ConsoleDestination()
      log.addDestination(console)
      
      // Initialize defaults
      if let defaultPrefsFile = Bundle.main.path(forResource: "defaultPrefs", ofType: "plist") {
         if let defaultPreferences = NSDictionary(contentsOfFile: defaultPrefsFile) as? [String: Any] {
            UserDefaults.standard.register(defaults: defaultPreferences)
         }
      }
      
      // Get SDK configuration override
      let sdkConfigurationOverride = UserDefaults.standard.string(forKey: "sdkConfigurationOverride")
      log.info("Using configuration override: \(String(describing: sdkConfigurationOverride))")
      
      // Initialize the TallyGo SDK
      TallyGoKit.initialize(withAccessToken: ACCESS_TOKEN,
                            simulatedCoordinate: nil,
                            configurationOverrideName: sdkConfigurationOverride)
      TallyGoKit.showsTraffic = true
//      TallyGoKit.allowsDebug = true

      StartingLocation.restoreSavedSettings()
      
      let accountManager = ServiceLocator.accountManager
      
      let showLeftPanel = accountManager.current.scan(false) { (alreadyShown, _) in
          !alreadyShown && launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] != nil
      }
      
      window?.rootViewController = nil
      
      _ = accountManager.current
         .distinctUntilChanged({ (status) -> String in status.token?.authToken ?? "" })
         .withLatestFrom(showLeftPanel, resultSelector: { ($0, $1) })
         .drive(onNext: { (status, showLeftPanel) in
            switch status {
            case .unauthorized:
               let loginController = LoginViewController.createInstance(
                  authService: ServiceLocator.authService
               )
               
               self.window?.rootViewController = loginController
               
            case .authorized:
               let homeController = HomeViewController(
                  dataManager: ServiceLocator.dataManager,
                  userService: ServiceLocator.userService,
                  alertService: ServiceLocator.alertService
               )
               
               let navigationController = UINavigationController(
                  rootViewController: homeController
               )
               
               self.showLeftPanel = showLeftPanel
               
               self.window?.rootViewController = navigationController
            }
         }
      )
      
      return true
   }
   
   func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      let tokenParts = deviceToken.map { data -> String in
         return String(format: "%02.2hhx", data)
      }
      
      print("The token parts are: \(tokenParts)")
      
      let token = tokenParts.joined()
      print("Device Token: \(token)")
      ServiceLocator.dataManager.deviceToken = token
   }
   
   func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
      print("Failed to register")
   }
   
   func handleNetworkConnection() {
      let messagesHandler = MessagesHandler()
      _ = Reactive<Reachability>.reachable
         .debug("reachability", trimOutput: false)
         .subscribe(onNext: { isReachable in
            messagesHandler.showNetworkTroublesIndicator(show: !isReachable)
         })
   }
   
   func setUserAgent() {
      let customAgent = "fdsu-mobile-client"
      var userAgent = UIWebView().stringByEvaluatingJavaScript(from: "navigator.userAgent") ?? ""
      
      if !userAgent.contains(customAgent) {
         userAgent.append(" " + customAgent)
      }
      
      log.info("App User Agent: \(userAgent)")
      
      UserDefaults.standard.register(defaults: ["UserAgent" : userAgent])
   }
   
   func registerForPushNotifications() {
      if #available(iOS 10.0, *) {
         UNUserNotificationCenter.current().requestAuthorization(
            options: [.alert, .sound, .badge]
         ) { (granted, error) in
            print("Permission granted: \(granted)")
            print("With error: \(error as Any)")
            
            guard granted else { return }
            
            self.getNotificationSettings()
         }
      } else {
         // Fallback on earlier versions
         let settings = UIUserNotificationSettings(
            types: [.sound, .alert, .badge],
            categories: nil
         )
         
         UIApplication.shared.registerUserNotificationSettings(settings)
         UIApplication.shared.registerForRemoteNotifications()
      }
   }
   
   func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
      NotificationCenter.default.post(
         name: RemoteNotificationArrived,
         object: nil,
         userInfo: nil
      )
      
      if application.applicationState == .inactive {
         NotificationCenter.default.post(name: NotificationShowLeftPanel, object: nil)
      }
   }
   
   @available(iOS 10.0, *)
   func getNotificationSettings() {
      UNUserNotificationCenter.current().getNotificationSettings { (settings) in
         print("Notification settings: \(settings)")
         
         guard settings.authorizationStatus == .authorized else { return }
         DispatchQueue.main.async {
            UIApplication.shared.registerForRemoteNotifications()
         }
      }
   }
}
