//
//  NSObject+.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 10/10/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import RxSwift

private struct AssociatedKeys {
   static var DisposeBagKey = "r_DisposeBagKey"
}

// TODO: Handle deallocation of dispose bag objects when its owner is deallocated
extension NSObject {
   /// Use this bag to dispose disposables upon the deallocation of the receiver.
   public var disposeBag: DisposeBag {
      if let disposeBag = objc_getAssociatedObject(self, &AssociatedKeys.DisposeBagKey) {
         return disposeBag as! DisposeBag
      } else {
         let disposeBag = DisposeBag()
         objc_setAssociatedObject(self, &AssociatedKeys.DisposeBagKey, disposeBag, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
         return disposeBag
      }
   }
}
