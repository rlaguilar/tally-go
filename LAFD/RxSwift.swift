//
//  RxSwift.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 9/8/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import RxSwift

extension NetworkClient {
//   func send<T> (
//      resource: Resource<T>,
//      requestHelper: RequestHelper = EmptyHelper(),
//      completion: @escaping (Result<T>) -> Void) -> Disposable {
   
   func send<T> (
      resource: Resource<T>,
      requestHelper: RequestHelper = EmptyHelper()) -> Observable<T> {
      
      return Observable.create({ (observer) -> Disposable in
         let task = self.send(resource: resource, requestHelper: requestHelper, completion: { (result) in
            switch result {
            case .failure(let error):
               observer.onError(error)
            case .success(let item):
               observer.onNext(item)
               observer.onCompleted()
            }
         })
         
         return Disposables.create {
            task.cancel()
         }
      })
   }
}
