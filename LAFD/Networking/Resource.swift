//
//  Resource.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/24/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct Resource<T> {
   let method: HTTPMethod
   let route: Route
   let params: [String: Any]
   let responseFormat: ResponseFormat
   let encoding: ParameterEncoding
   
   init(route: Route,
        method: HTTPMethod = .get,
        params: [String: Any] = [:],
        encoding: ParameterEncoding = JSONEncoding.default,
        responseFormat: ResponseFormat
      ) {
      self.route = route
      self.method = method
      self.params = params
      self.encoding = encoding
      self.responseFormat = responseFormat
   }
   
   func parse(response: Data) -> Result<T> {
      switch responseFormat {
      case .data(let mapper):
         let value = mapper(response)
         return Result<T>.success(value)
      case .json(let mapper):
         let json = JSON(data: response)
         
         print("Processing response: \(json)")
         
         if json["code"].exists() {
            let message = json["message"].stringValue
            return Result.failure(AppError(message: message))
         }
         else {
            do {
               let value = try mapper(json)
               return Result.success(value)
            }
            catch {
               let appError = AppError(error: error)
               return Result.failure(appError)
            }
         }
      }
   }
   
   enum ResponseFormat {
      case data((Data) -> T)
      case json((JSON) throws -> T)
   }
}
