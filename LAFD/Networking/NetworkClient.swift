//
//  NetworkClient.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/24/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class NetworkClient {
   lazy var manager: SessionManager = {
      // Create the server trust policies
      let serverTrustPolicies: [String: ServerTrustPolicy] = [
         "10.10.10.199": .disableEvaluation
      ]
      
      // Create custom manager
      let configuration = URLSessionConfiguration.default
      configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
      let manager = Alamofire.SessionManager(
         configuration: URLSessionConfiguration.default,
         serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
      )
      
      return manager
   }()
   
   let helper: RequestHelper
   
   init(helper: RequestHelper) {
      self.helper = helper
   }
   
   func send<T> (
      resource: Resource<T>,
      requestHelper: RequestHelper = EmptyHelper(),
      completion: @escaping (Result<T>) -> Void) -> DataRequest {
      
      let compoundHelper = CompoundHelper(helpers: self.helper, requestHelper)
      
      compoundHelper.beforeSend()
      
      let task = manager.request(
         resource.route,
         method: resource.method,
         parameters: resource.params,
         encoding: resource.encoding,
         headers: compoundHelper.headers
      )
      .validate(statusCode: (100 ..< 500))
      .responseData { (response) in
         switch response.result {
         case .failure(let error):
            compoundHelper.afterError(error: error, code: response.response?.statusCode)
            let result = Result<T>.failure(AppError(error: error))
            completion(result)
         case .success(let data):
            let result = resource.parse(response: data)
            
            switch result {
            case .failure(let error):
               compoundHelper.afterError(error: error, code: response.response?.statusCode)
            case .success(let value):
               compoundHelper.afterSuccess(value: value)
            }
            
            completion(result)
         }
      }
      
      
      print(task.debugDescription)
      
      task.resume()
      
      return task
   }
//   
//   func send(
//      route: Route,
//      helper: RequestHelper,
//      method: HTTPMethod,
//      encoding: URLEncoding,
//      headers: HTTPHeaders) -> DataRequest {
////      return request(route, method: method, parameters: helper.params, encoding: encoding, headers: headers)
//   }
}
