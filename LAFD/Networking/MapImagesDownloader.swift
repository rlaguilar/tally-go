//
//  ImageDownloader.swift
//  First Due Mobile
//
//  Created by Reynaldo Aguilar on 11/1/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class ImageDownloader {
   let disposeBag = DisposeBag()
   let session: URLSession
   
   let currentSymbols: Observable<[MapSymbol]>
   
   init(layerService: MapLayerService) {
      let config = URLSessionConfiguration.default
      let cache = URLCache(
         memoryCapacity: 10*1024*1024, // 10MB
         diskCapacity: 10*1024*1024,
         diskPath: "layer-icons"
      )
      
      config.urlCache = cache
      config.httpMaximumConnectionsPerHost = 5
      session = URLSession(configuration: config)
      
      currentSymbols = Observable<Int>.timer(
         1,
         period: 60,
         scheduler: MainScheduler.instance
         ).flatMapLatest { _ in layerService.getLayersSymbol() }
         .share(replay: 1, scope: SubjectLifetimeScope.forever)
      
      currentSymbols.subscribe().disposed(by: disposeBag)
   }
   
   func getImage(forCode code: String, size: CGSize) -> Single<UIImage> {
      return currentSymbols.map { symbols in symbols.first { $0.code == code } }
         .filter { $0 != nil }
         .map { $0! }
         .take(1)
         .flatMap { symbol in self.getImage(url: symbol.url, size: size) }
         .asSingle()
   }
   
   private func getImage(url: URL, size: CGSize) -> Single<UIImage> {
      let request = URLRequest(url: url)
      
      return session.rx.data(request: request)
         .retry(3)
         .map { data -> UIImage in
            guard let image = UIImage(data: data) else {
               throw AppError.message("Invalid image data")
            }
            
            return image
         }.map { self.resize(image: $0, size: size) }
         .asSingle()
   }
   
   private func resize(image: UIImage, size: CGSize) -> UIImage {
      UIGraphicsBeginImageContextWithOptions(size, false, 0)
      image.draw(in: CGRect(origin: .zero, size: size))
      
      guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
         fatalError()
      }
      
      return image
   }
}
