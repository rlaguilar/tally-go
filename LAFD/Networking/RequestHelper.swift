//
//  RequestHelper.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/24/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation

protocol RequestHelper {
   var headers: [String: String] { get }
   
   func afterError(error: Error, code: Int?)
   func afterSuccess(value: Any)
}

extension RequestHelper {
   var headers: [String: String] { return [:] }
   
   func beforeSend() {}
   func afterError(error: Error, code: Int?) {}
   func afterSuccess(value: Any) {}
}

class EmptyHelper: RequestHelper {
   static var instance: EmptyHelper = EmptyHelper()
}

class CompoundHelper: RequestHelper {
   let helpers: [RequestHelper]
   
   var headers: [String : String] {
      var result: [String: String] = [:]
      
      helpers.flatMap { $0.headers }.forEach { result[$0] = $1 }
      
      return result
   }
   
   init(helpers: RequestHelper...) {
      self.helpers = helpers
   }
   
   func beforeSend() {
      helpers.forEach { $0.beforeSend() }
   }
   
   func afterError(error: Error, code: Int?) {
      helpers.forEach { $0.afterError(error: error, code: code) }
   }
   
   func afterSuccess(value: Any) {
      helpers.forEach { $0.afterSuccess(value: value) }
   }
}
