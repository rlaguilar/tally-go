//
//  Routes.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/24/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import Alamofire

enum Route {
   private var baseURL: URL {
      #if DEV
         let url = "https://sizeup-dev.firstduesizeup.com"
      #elseif DEMO
         let url = "https://demo.firstduesizeup.com"
      #else
         let url = "https://sizeup.firstduesizeup.com"
      #endif
      
      log.info("The base url is: \(url)")
      let apiUrl = "\(url)/api/v1/"
      let webUrl = url
      
      switch self {
      case .settings, .preplanWeb, .commingSoonFeatures:
         return URL(string: webUrl)!
      default:
         return URL(string: apiUrl)!
      }
   }
   
   case commingSoonFeatures
   case getAlerts
   case hydrants
   case login
   case preplanList
   case preplanWeb
   case removeToken
   case settings
   case story(alertId: Int)
   case symbols
   case trackLocations
   case updateTrackingLocation
   case updateToken
   
   private var relativePath: String {
      switch self {
      case .commingSoonFeatures:
         return "featureRequest"
      case .getAlerts:
         return "dispatches"
      case .hydrants:
         return "hydrants"
      case .login:
         return "auth/token"
      case .preplanList:
         return "preplans"
      case .preplanWeb:
         return "preplan"
      case .removeToken:
         return "notifications/unsubscribe"
      case .settings:
         return "account"
      case .story(alertId: let id):
         return "addresses/\(id)/story"
      case .symbols:
         return "icons"
      case .trackLocations:
         return "device-locations"
      case .updateTrackingLocation:
         return "device-locations"
      case .updateToken:
         return "notifications/subscribe"
      }
   }
   
   var absoluteUrl: URL {
      return baseURL.appendingPathComponent(relativePath)
   }
}

extension Route: URLConvertible {
   func asURL() throws -> URL {
      return self.absoluteUrl
   }
}
