//
//  IncidentAnnotation.swift
//  LAFD
//
//  Created by Anthony Picciano on 1/25/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import UIKit
import Mapbox

class AlertAnnotation: NSObject, MGLAnnotation {
   
   var title: String?
   var subtitle: String?
   var crossStreet: String?
   var addressId: Int?
   var coordinate: CLLocationCoordinate2D
   var alert: Alert
   var url: URL?
   
   init(alert: Alert) throws {
      self.alert = alert
   
      guard let coordinate = alert.coordinates else {
         throw AppError.invalidOperation(
            message: "unable to create annotation from alert without coordinates"
         )
      }
      
      self.coordinate = coordinate
      
      title = "Incident: \(alert.id)"
      
      subtitle = alert.address
      crossStreet = alert.city != nil ? "@\(alert.city!)" : nil
      addressId = alert.addressId
      url = alert.url
   }
   
   func imageOfMapMarker() -> UIImage {
      return LAFDStyleKit.imageOfLAFDIncident
   }
   
   func mapMarkerReuseKey() -> String {
      return LAFDIncidentReuseIdentifier
   }
   
   func calloutView() -> AlertCalloutView {
      return AlertCalloutView(representedObject: self)
   }
   
}
