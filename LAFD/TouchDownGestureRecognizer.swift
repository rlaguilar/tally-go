//
//  TouchDownGestureRecognizer.swift
//  LAFD
//
//  Created by Anthony Picciano on 11/18/16.
//  Copyright © 2016 TALLYGO. All rights reserved.
//

import UIKit
import UIKit.UIGestureRecognizerSubclass

class TouchDownGestureRecognizer: UIGestureRecognizer {
    
    override func touchesBegan(_: Set<UITouch>, with: UIEvent) {
        if state == .possible {
            state = .recognized
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
        state = .failed
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
        state = .failed
    }

}
