//
//  IncidentsListViewController.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/7/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import RxSwift
import RxCocoa

let NotificationListDidSelectItem = Notification.Name("NotificationListDidSelectItem")
let IncidentNotificationKey = "IncidentNotificationKey"

let RollToAlertNotification = Notification.Name("RollToAlertNotification")
let ShowSizeupNotification = Notification.Name("ShowSizeupNotifiaction")
let AlertNotificationKey = "AlertNotifiactionKey"

let AlertItemsChanged = Notification.Name("NotificationAlertItemsChanged")
let AlertItemsKey = "AlertItemsKey"

class IncidentsListViewController: ReactiveViewController, UITableViewDataSource, UITableViewDelegate {
   var tableView: UITableView!
   
   var alerts: Variable<[AlertViewModel]> = Variable([])
   
   let alertService: AlertService

   let fetcher: AlertsFetcher
   
   lazy var manualReloadView: UIView = {
      let label = UILabel()
      label.text = "Failed loading incidents"
      label.textAlignment = .center
      label.textColor = .white
      let button = UIButton()
      button.setTitle("Reload", for: .normal)
      button.sizeToFit()
      let stack = UIStackView(arrangedSubviews: [label, button])
      stack.axis = .vertical
      stack.spacing = 4
      
      button.rx.tap.bind(to: self.fetcher.manualTrigger)
         .disposed(by: disposeBag)
      
      let container = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 70))
      container.addSubview(stack)
      let topMargin: CGFloat = 10
      stack.frame = container.bounds.insetBy(dx: 0, dy: topMargin / 2).offsetBy(dx: 0, dy: topMargin)
      stack.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      return container
   }()
   
   lazy var headerView: UIView = {
      let indicatorView = NVActivityIndicatorView(
         frame: CGRect(x: 0, y: 0, width: 40, height: 40),
         type: .ballPulse,
         color: UIColor.white,
         padding: 0
      )
      indicatorView.startAnimating()
   
      let container = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
      container.backgroundColor = .clear
      indicatorView.center = container.center
      container.addSubview(indicatorView)
      indicatorView.autoresizingMask = [
         .flexibleTopMargin,
         .flexibleBottomMargin,
         .flexibleLeftMargin,
         .flexibleRightMargin
      ]
      
      return container
   }()
   
   lazy var unreadItems: Observable<[AlertViewModel]> = {
      return self.alerts.asObservable().map { items in
         items.filter { !$0.isRead }
      }
   }()
   
   init(alertService: AlertService) {
      self.alertService = alertService
      fetcher = AlertsFetcher(alertService: alertService, pageSize: 100)
      super.init(nibName: nil, bundle: nil)
   }
   
   required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      // Do any additional setup after loading the view.
      let gray: CGFloat = 51.0 / 255
      self.view.backgroundColor = UIColor(red: gray, green: gray, blue: gray, alpha: 1)
      
      // setting up the header view
      self.title = "Latest Incidents"
      self.navigationController?.navigationBar.barStyle = .black
      let navBar = self.navigationController?.navigationBar
      navBar?.tintColor = .white
      
      // setting up table view
      tableView = UITableView()
      self.view.addSubview(tableView)
      tableView.translatesAutoresizingMaskIntoConstraints = false
      tableView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
      tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
      tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
      tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
      
      let cellNib = UINib(nibName: IncidentNotificationCell.identifier, bundle: .main)
      tableView.register(cellNib, forCellReuseIdentifier: IncidentNotificationCell.identifier)
      
      tableView.rowHeight = UITableViewAutomaticDimension
      tableView.estimatedRowHeight = 50
      
      tableView.dataSource = self
      tableView.delegate = self
      tableView.backgroundColor = .clear
      tableView.allowsMultipleSelection = false
      tableView.indicatorStyle = .white
      
      self.whenViewAppeared.bind { [unowned self] in
         var insets = self.tableView.contentInset
         
         // TODO: This value is to allow the list to scroll beyond the incidents
         // button
         insets.bottom = 112
         self.tableView.contentInset = insets
         self.tableView.scrollIndicatorInsets = insets
      }.disposed(by: disposeBag)
      
      let dataManager = ServiceLocator.dataManager
      
      fetcher.isLoading.asObservable()
         .debounce(0.3, scheduler: MainScheduler.instance)
         .subscribe(onNext: { [unowned self] isLoading in
            if isLoading {
               self.tableView.tableHeaderView = self.headerView
            }
            else {
               if self.tableView.tableHeaderView == self.headerView {
                  self.tableView.tableHeaderView = nil
               }
            }
         }).disposed(by: disposeBag)
      
      fetcher.timeline
         .subscribe(
         onNext: { [unowned self] result in
            switch result {
            case .failure:
               self.tableView.tableHeaderView = self.manualReloadView
            case .success(let items):
               self.tableView.tableHeaderView = nil
               
               let newViewModels = items.map { alert in
                  AlertViewModel(
                     alert: alert,
                     isRead: dataManager.lastViewedAlertId >= alert.id
                  )
               }
               self.alerts.value.insert(contentsOf: newViewModels, at: 0)
               
               let indexPaths = items.enumerated().map { (idx, _) in
                  IndexPath(row: idx, section: 0)
               }
               
               self.tableView.insertRows(at: indexPaths, with: UITableViewRowAnimation.top)
               
               NotificationCenter.default.post(
                  name: AlertItemsChanged,
                  object: nil,
                  userInfo: [AlertItemsKey: items]
               )
            }
            
         }).disposed(by: self.disposeBag)
      
      fetcher.timeline
         .skip(1)
         .subscribe(onNext: { result in
         guard case let Result.success(items) = result, !items.isEmpty else { return }
         
         MessagesHandler.showInfo(message: "New incidents arrived")
         }).disposed(by: self.disposeBag)
      
      self.alerts.asObservable().map { $0.isEmpty }
         .distinctUntilChanged()
         .subscribe(onNext: { [unowned self] isEmpty in
            if isEmpty {
               let label = UILabel(frame:
                  CGRect(x: 0, y: 0, width: self.tableView.bounds.width, height: 300)
               )
               
               label.text = "No incidents recently"
               label.textColor = .white
               label.textAlignment = .center
               label.numberOfLines = 0
               
               self.tableView.tableFooterView = label
            }
            else {
               self.tableView.tableFooterView = UIView()
            }
         }).disposed(by: disposeBag)
   }
   
   func numberOfSections(in tableView: UITableView) -> Int {
      return 1
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return self.alerts.value.count
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: IncidentNotificationCell.identifier, for: indexPath) as! IncidentNotificationCell
      
      let item = alerts.value[indexPath.row]
      
      cell.item = item
      
      let center = NotificationCenter.default
      
      // Use these observables to void the animation stoping when any of the
      // following conditions arises:
      // 1. The application goes to background and then becomes active
      // 2. Some view (vc) is presented over the animated view
      Observable.of(
         whenViewAppeared,
         center.rx.notification(.UIApplicationDidBecomeActive).map { _ in }
         )
         .merge()
         .map { item.isRead }
         .subscribe(onNext: { [unowned cell] isRead in
            let gray: CGFloat = CGFloat(51) / 255.0
            cell.contentView.layer.removeAllAnimations()
            cell.contentView.backgroundColor = UIColor(white: gray, alpha: 1)
            
            if !isRead {
               UIView.animate(
                  withDuration: 0.2,
                  delay: 0,
                  options: [.repeat, .autoreverse, .allowUserInteraction],
                  animations: {
                     cell.contentView.backgroundColor = UIColor.black
                  },
                  completion: nil
               )
            }
         }).disposed(by: cell.reuseBag)
      
      return cell
   }
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      let vc = AlertDetailsViewController.instantiate(from: .Main)
      let alertVM = self.alerts.value[indexPath.row]
      self.markAsRead(alertAt: indexPath.row)
      
      vc.item = alertVM
      self.show(vc, sender: self)
      
      self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
      
      NotificationCenter.default.post(
         name: NotificationListDidSelectItem,
         object: nil,
         userInfo: [IncidentNotificationKey: alertVM.alert]
      )
   }
   
   func markAsRead(alertAt index: Int) {
      markAsRead(alertsAt: [index])
   }
   
   func markAllAsRead() {
      self.markAsRead(alertsAt: (0 ..< self.alerts.value.count).map { $0 })
   }
   
   func markAsRead(alertsAt indexes: [Int]) {
      let itemsToMark = indexes.filter { !alerts.value[$0].isRead }
      
      let maxMarkedItem = itemsToMark
         .map { alerts.value[$0].alert.id }
         .reduce(0, max)
      
      var dataManager = ServiceLocator.dataManager
      
      if dataManager.lastViewedAlertId < maxMarkedItem {
         dataManager.lastViewedAlertId = maxMarkedItem
      }
      
      itemsToMark.forEach { (index) in
         let alert = self.alerts.value[index]
         alert.isRead = true
         // HACK: force the variable to trigger an update
         alerts.value[index] = alert
      }
      
      let indexPaths = itemsToMark.map { IndexPath(row: $0, section: 0) }
      self.tableView.reloadRows(at: indexPaths, with: .fade)
   }
}

class AlertViewModel {
   let alert: Alert
   var isRead: Bool
   var status: Variable<AlertResponseStatus> = Variable(.unknown)
   
   init(alert: Alert, isRead: Bool) {
      self.alert = alert
      self.isRead = isRead
   }
}
