//
//  SymbolLayerAnnotation.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 9/21/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation
import Mapbox

class SymbolLayerAnnotation: MGLPointAnnotation {
   let code: String

   init(code: String) {
      self.code = code
      super.init()
   }
   
   required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
}
