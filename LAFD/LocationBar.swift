//
//  LocationBar.swift
//  LAFD
//
//  Created by Anthony Picciano on 11/16/16.
//  Copyright © 2016 TALLYGO. All rights reserved.
//

import UIKit
import TallyGoKit

class LocationBar: UIButton {
    
    var address: TGAddress? {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var totalDurationInMinutes: Double? {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        if let addressLine = address?.displayAddress {
            if let locationName = address?.locationName, locationName != "USA.PointAddress", locationName != "USA.StreetName" {
                LAFDStyleKit.drawLocationBar(frame: bounds, addressLine: addressLine, locationName: locationName, travelTime: travelTime)
            } else {
                LAFDStyleKit.drawLocationBarSingleLine(frame: bounds, addressLine: addressLine, travelTime: travelTime)
            }
            isHidden = false
        } else {
            isHidden = true
        }
    }
    
    var travelTime: String {
        var result = "—"
        
        if let totalDurationInMinutes = totalDurationInMinutes {
            let minutes = Int(ceil(totalDurationInMinutes))
            result = String(format: "%i min", minutes)
        }
        
        return result
    }

}
