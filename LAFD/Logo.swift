//
//  Logo.swift
//  LAFD
//
//  Created by Anthony Picciano on 11/14/16.
//  Copyright © 2016 TALLYGO. All rights reserved.
//

import UIKit

class Logo: UIView {
    
    @IBInspectable var simulated: Bool = false {
        didSet { setNeedsDisplay() }
    }

    override func draw(_ rect: CGRect) {
        if simulated {
//            LAFDStyleKit.drawTallyGoLogoOutlineSim()
        } else {
//            LAFDStyleKit.drawTallyGoLogoOutline()
        }
    }

}
