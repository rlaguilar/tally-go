//
//  SplitViewController.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/7/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import UIKit

class SplitViewController: ReactiveViewController {
   let leftPanelWidth: CGFloat = UIDevice.current.userInterfaceIdiom == .pad
      ? 320 : UIScreen.main.bounds.width
   let rightPanelWidth: CGFloat = UIDevice.current.userInterfaceIdiom == .pad
      ? 320 : UIScreen.main.bounds.width
   
   let leftVC: UIViewController
   let middleVC: UIViewController
   let rightVC: UIViewController
   
   var leftToggleButton: UIButton!
   var rightToggleButton: UIButton!
   
   var leftPanelLeadingConstraint: NSLayoutConstraint!
   var rightPanelTrailingConstraint: NSLayoutConstraint!
   
   var isLeftOpened: Bool = false
   var isRightOpened: Bool = false
   
   init(leftVC: UIViewController, middleVC: UIViewController, rightVC: UIViewController) {
      self.leftVC = leftVC
      self.middleVC = middleVC
      self.rightVC = rightVC
      super.init(nibName: nil, bundle: nil)
   }
   
   required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.navigationController?.navigationBar.barStyle = UIBarStyle.black
      self.navigationController?.navigationBar.tintColor = .white
      
      // setting up left panel
      leftVC.willMove(toParentViewController: self)
      self.view.addSubview(leftVC.view)
      self.addChildViewController(leftVC)
      leftVC.didMove(toParentViewController: self)
      
      leftVC.view.translatesAutoresizingMaskIntoConstraints = false
      self.leftPanelLeadingConstraint = leftVC.view.leadingAnchor.constraint(
         equalTo: self.view.leadingAnchor
      )
      self.leftPanelLeadingConstraint.isActive = true

      leftVC.view.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
      leftVC.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
      leftVC.view.widthAnchor.constraint(equalToConstant: leftPanelWidth).isActive = true
      
      // setting up right panel
      rightVC.willMove(toParentViewController: self)
      self.view.addSubview(rightVC.view)
      self.addChildViewController(rightVC)
      rightVC.didMove(toParentViewController: self)
      
      rightVC.view.translatesAutoresizingMaskIntoConstraints = false
      self.rightPanelTrailingConstraint = rightVC.view.trailingAnchor.constraint(
         equalTo: self.view.trailingAnchor
      )
      self.rightPanelTrailingConstraint.isActive = true
      

      rightVC.view.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
      rightVC.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
      rightVC.view.widthAnchor.constraint(equalToConstant: rightPanelWidth).isActive = true
      
      // setting up middle panel
      middleVC.willMove(toParentViewController: self)
      self.view.insertSubview(middleVC.view, at: 0)
      self.addChildViewController(middleVC)
      middleVC.didMove(toParentViewController: self)
      
      middleVC.view.translatesAutoresizingMaskIntoConstraints = false
      middleVC.view.leadingAnchor.constraint(equalTo: leftVC.view.trailingAnchor).isActive = true
      middleVC.view.topAnchor.constraint(equalTo: leftVC.view.topAnchor).isActive = true
      middleVC.view.trailingAnchor.constraint(equalTo: rightVC.view.leadingAnchor).isActive = true
      middleVC.view.bottomAnchor.constraint(equalTo: leftVC.view.bottomAnchor).isActive = true
      
      
      // adding left panel toggle button
      let leftPanelToggle = UIButton(type: .system)
      self.view.addSubview(leftPanelToggle)
      leftPanelToggle.setImage(#imageLiteral(resourceName: "ic_ray"), for: .normal)
      leftPanelToggle.tintColor = .white
      leftPanelToggle.backgroundColor = UIColor(red: 229.0 / 255, green: 32.0 / 255, blue: 4.0 / 255, alpha: 1)
      let buttonSide: CGFloat = 60
      let buttonBottomMargin: CGFloat = 40
      leftPanelToggle.layer.cornerRadius = buttonSide / 2
      leftPanelToggle.layer.shadowOpacity = 0.4
      leftPanelToggle.layer.shadowOffset = .zero
      
      leftPanelToggle.translatesAutoresizingMaskIntoConstraints = false
      leftPanelToggle.widthAnchor.constraint(equalToConstant: buttonSide).isActive = true
      leftPanelToggle.heightAnchor.constraint(equalTo: leftPanelToggle.widthAnchor).isActive = true
      let leftButtonLeading = leftPanelToggle.leadingAnchor.constraint(equalTo: leftVC.view.trailingAnchor, constant: 40)
      leftButtonLeading.priority = UILayoutPriority.defaultHigh
      leftButtonLeading.isActive = true
      leftPanelToggle.trailingAnchor.constraint(lessThanOrEqualTo: self.view.trailingAnchor, constant: -10).isActive = true
      leftPanelToggle.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -buttonBottomMargin).isActive = true
      
      leftPanelToggle.addTarget(self, action: #selector(toggleLeftPanelAction(_:)), for: .touchUpInside)
      
      // adding right panel toggle button
      let rightPanelToggle = UIButton(type: .system)
//      rightPanelToggle.isHidden = true
      self.view.addSubview(rightPanelToggle)
      rightPanelToggle.setImage(#imageLiteral(resourceName: "ic_menu"), for: .normal)
      rightPanelToggle.tintColor = .white
      
      let gray: CGFloat = 51.0 / 255
      rightPanelToggle.backgroundColor = UIColor(red: gray, green: gray, blue: gray, alpha: 1)
      
      rightPanelToggle.layer.cornerRadius = buttonSide / 2
      rightPanelToggle.layer.shadowOpacity = 0.4
      rightPanelToggle.layer.shadowOffset = .zero
      
      rightPanelToggle.translatesAutoresizingMaskIntoConstraints = false
      rightPanelToggle.widthAnchor.constraint(equalToConstant: buttonSide).isActive = true
      rightPanelToggle.heightAnchor.constraint(equalTo: rightPanelToggle.widthAnchor).isActive = true
      let rightButtonTrailing = rightPanelToggle.trailingAnchor.constraint(equalTo: rightVC.view.leadingAnchor, constant: -40)
      rightButtonTrailing.priority = UILayoutPriority.defaultHigh
      rightButtonTrailing.isActive = true
      rightPanelToggle.leadingAnchor.constraint(greaterThanOrEqualTo: self.view.leadingAnchor, constant: 10).isActive = true
      
      rightPanelToggle.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -40).isActive = true
      
      rightPanelToggle.addTarget(self, action: #selector(toggleRightPanelAction(_:)), for: .touchUpInside)
      
      self.leftToggleButton = leftPanelToggle
      self.rightToggleButton = rightPanelToggle
      
      self.showLeftPanel(show: false, animated: false)
      self.showRightPanel(show: false, animated: false)
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      self.navigationController?.setNavigationBarHidden(true, animated: true)
   }
   
   func showToggleButtons(show: Bool) {
      self.leftToggleButton.isHidden = !show
      self.rightToggleButton.isHidden = !show
      
      if !show {
         self.showLeftPanel(show: false, animated: true)
         self.showRightPanel(show: false, animated: true)
      }
   }
   
   @objc func toggleLeftPanelAction(_ sender: UIButton) {
      if sender.isUserInteractionEnabled {
         self.showLeftPanel(show: !self.isLeftOpened, animated: true)
      }
   }
   
   @objc func toggleRightPanelAction(_ sender: UIButton) {
      if sender.isUserInteractionEnabled {
         self.showRightPanel(show: !self.isRightOpened, animated: true)
      }
   }
   
   func showLeftPanel(show: Bool, animated: Bool) {
      self.rightToggleButton.isUserInteractionEnabled = !show
      
      if UIDevice.current.userInterfaceIdiom == .phone {
         self.middleVC.view.isUserInteractionEnabled = !show
      }
      
      let block: () -> Void = { [weak self] in
         guard let `self` = self else { return }
         self.leftPanelLeadingConstraint.constant = show ? 0 : -self.leftPanelWidth
         self.rightToggleButton.alpha = show ? 0 : 1
      }
      
      let willShow = show;
      
      if animated {
         var mapImageView: UIImageView? = nil
         
         if willShow && self == UIApplication.topViewController() {
            if let mapImage = UIApplication.generateScreenshot(fromView: self.middleVC.view) {
               let imageView = UIImageView(image: mapImage)
               self.middleVC.view.addSubview(imageView)
               imageView.frame = self.middleVC.view.bounds
               imageView.autoresizingMask = [.flexibleHeight, .flexibleLeftMargin, .flexibleRightMargin]
               mapImageView = imageView
            }
         }
         
         UIView.animate(withDuration: 0.3, animations: { [weak self] in
            guard let `self` = self else { return }
            block()
            self.view.layoutIfNeeded()
         }, completion: { (_) in
            mapImageView?.removeFromSuperview()
         })
      }
      else {
         block()
      }
      
      self.isLeftOpened = show
   }
   
   func showRightPanel(show: Bool, animated: Bool) {
      self.leftToggleButton.isUserInteractionEnabled = !show
      
      if UIDevice.current.userInterfaceIdiom == .phone {
         self.middleVC.view.isUserInteractionEnabled = !show
      }
      
      let block: () -> Void = { [weak self] in
         guard let `self` = self else { return }
         self.rightPanelTrailingConstraint.constant = show ? 0 : self.rightPanelWidth
         self.leftToggleButton.alpha = show ? 0 : 1
      }
      
      let willShow = show;
      
      if animated {
         var mapImageView: UIImageView? = nil
         
         if willShow && self == UIApplication.topViewController() {
            if let mapImage = UIApplication.generateScreenshot(fromView: self.middleVC.view) {
               let imageView = UIImageView(image: mapImage)
               self.middleVC.view.addSubview(imageView)
               imageView.frame = self.middleVC.view.bounds
               imageView.autoresizingMask = [.flexibleHeight, .flexibleLeftMargin, .flexibleRightMargin]
               mapImageView = imageView
            }
         }
         
         UIView.animate(withDuration: 0.3, animations: { [weak self] in
            guard let `self` = self else { return }
            block()
            self.view.layoutIfNeeded()
         }, completion: { (_) in
            mapImageView?.removeFromSuperview()
         })
      }
      else {
         block()
      }
      
      self.isRightOpened = show
   }
}
