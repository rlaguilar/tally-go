//
//  OptionsButton.swift
//  LAFD
//
//  Created by Anthony Picciano on 11/15/16.
//  Copyright © 2016 TALLYGO. All rights reserved.
//

import UIKit

let OptionsButtonSize: CGSize = CGSize(width: 64, height: 64)
let OptionsButtonTag = 994

class OptionsButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commintInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commintInit()
    }
    func commintInit(){
        tag = OptionsButtonTag
    }
    
    override var isHighlighted: Bool {
        didSet { setNeedsDisplay() }
    }

    override func draw(_ rect: CGRect) {
        LAFDStyleKit.drawOptionsButton(highlighted: isHighlighted)
    }

}
