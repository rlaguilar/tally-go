//
//  StartingLocation.swift
//  LAFD
//
//  Created by Anthony Picciano on 11/23/16.
//  Copyright © 2016 TALLYGO. All rights reserved.
//

import Foundation
import TallyGoKit
import CoreLocation

let SavedStartingLocationIndexKey = "SavedStartingLocationIndexKey"
let SavedDrivingSimulatorEnabledKey = "SavedDrivingSimulatorEnabledKey"

struct StartingLocation {
    
    let name: String
    let coordinate: CLLocationCoordinate2D
    
    static let locations = [ StartingLocation(name: "LA Center", coordinate: CLLocationCoordinate2DMake(34.050000, -118.250000)),
                             StartingLocation(name: "USC", coordinate: CLLocationCoordinate2DMake(34.0204372, -118.2810193)),
                             StartingLocation(name: "LAX Airport", coordinate: CLLocationCoordinate2DMake(33.943463, -118.408171)),
                             StartingLocation(name: "West Hollywood", coordinate: CLLocationCoordinate2DMake(34.071413, -118.397388)),
                             StartingLocation(name: "Pasadena", coordinate: CLLocationCoordinate2DMake(34.140617, -118.149600)),
                             StartingLocation(name: "Irvine", coordinate: CLLocationCoordinate2DMake(33.6871232, -117.8442915)),
                             StartingLocation(name: "Redlands", coordinate: CLLocationCoordinate2DMake(34.0560768, -117.1978743)),
                             StartingLocation(name: "New York", coordinate: CLLocationCoordinate2DMake(40.727634277096257, -73.702809052286341))]
    
    static var `default`: StartingLocation {
        get {
            return locations.first!
        }
    }
    
    static func location(for coordinate: CLLocationCoordinate2D) -> StartingLocation? {
        for location in locations {
            if location.coordinate.latitude == coordinate.latitude &&
                location.coordinate.longitude == coordinate.longitude {
                return location
            }
        }
        
        return nil
    }
    
    static var savedDrivingSimulatorEnabled: Bool {
        
        get {
            return UserDefaults.standard.bool(forKey: SavedDrivingSimulatorEnabledKey)
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: SavedDrivingSimulatorEnabledKey)
            UserDefaults.standard.synchronize()
        }
        
    }
    
    static var savedStartingLocation: StartingLocation {
        
        get {
            let index = UserDefaults.standard.integer(forKey: SavedStartingLocationIndexKey)
            return StartingLocation.locations[index]
        }
        
        set {
            let index = StartingLocation.index(of: newValue)
            UserDefaults.standard.set(index, forKey: SavedStartingLocationIndexKey)
            UserDefaults.standard.synchronize()
            TallyGoKit.simulatedCoordinate = newValue.coordinate
            StartingLocation.announceSimulator()
        }
        
    }
    
    static fileprivate func index(of startingLocation: StartingLocation) -> Int {
        for (index, location) in StartingLocation.locations.enumerated() {
            if location.name == startingLocation.name {
                return index
            }
        }
        
        return 0
    }
    
    static func restoreSavedSettings() {
        if savedDrivingSimulatorEnabled {
            TallyGoKit.simulatedCoordinate = savedStartingLocation.coordinate
            StartingLocation.announceSimulator()
        } else {
            TallyGoKit.simulatedCoordinate = nil
        }
    }
    
    static func announceSimulator() {
        TGVoiceSynthesis.shared.say(text: "Driving simulator is now enabled.")
    }
    
}
