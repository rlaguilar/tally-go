//
//  GreenButton.swift
//  LAFD
//
//  Created by Anthony Picciano on 11/17/16.
//  Copyright © 2016 TALLYGO. All rights reserved.
//

import UIKit
import TallyGoKit

class GreenButton: UIButton {

    @IBInspectable var buttonText: String = "TallyGo!" {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var fontSize: CGFloat = 36 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        LAFDStyleKit.drawGreenButton(frame: bounds, highlighted: isHighlighted, buttonText: buttonText, fontSize: traitCollection.horizontalSizeClass == .compact ? fontSize/1.5 : fontSize)
    }

}
