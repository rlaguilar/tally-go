//
//  DarkButton.swift
//  LAFD
//
//  Created by Anthony Picciano on 11/15/16.
//  Copyright © 2016 TALLYGO. All rights reserved.
//

import UIKit
import TallyGoKit

class DarkButton: UIButton {
    
    @IBInspectable var buttonText: String = "Action" {
        didSet {
            setNeedsDisplay()
        }
    }
    @IBInspectable var fontSize: CGFloat = 12 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        LAFDStyleKit.drawDarkButton(frame: bounds, highlighted: isHighlighted, buttonText: buttonText, fontSize: traitCollection.horizontalSizeClass == .compact ? fontSize/1.5 : fontSize)
    }

}
