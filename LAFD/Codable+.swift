//
//  Codable+.swift
//  First Due Mobile
//
//  Created by Reynaldo Aguilar on 12/14/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation

extension Decodable {
   static func fromJSON(data: Data) throws -> Self {
      let decoder = JSONDecoder()
      let result = try decoder.decode(self, from: data)
      return result
   }
}

extension Encodable {
   func toJSON(pretty: Bool) throws -> Data {
      let encoder = JSONEncoder()
      let data = try encoder.encode(self)
      return data
   }
}
