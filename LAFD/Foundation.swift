//
//  Foundation.swift
//  LAFD
//
//  Created by Reynaldo Aguilar on 8/9/17.
//  Copyright © 2017 TALLYGO. All rights reserved.
//

import Foundation

extension Date {
   static var format: String {
      return "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
   }
   
   func formatToUniversal() -> String {
      let formatter = DateFormatter()
      //""yyyy-MM-dd'T'HH:mm:ssxxx""
      formatter.dateFormat = Date.format
      return formatter.string(from: self)
   }
   
   init?(universalStr str: String) {
      let formatter = DateFormatter()
      formatter.dateFormat = Date.format
      
      if let date = formatter.date(from: str) {
         self.init(timeInterval: 0, since: date)
      }
      else {
         return nil
      }
   }
   
   func formatToReadable() -> String {
      let formatter = DateFormatter()
      formatter.dateFormat = "HH:mm:ss MMM, dd"
      return formatter.string(from: self)
   }
}

extension String {
   func toRequired(_ replacement: String = "---") -> String {
      return self.isEmpty ? replacement : self
   }
}

extension Optional where Wrapped == String {
   func toRequired(_ replacement: String = "---") -> String {
      return (self ?? "").toRequired(replacement)
   }
}

public extension Sequence {
   func group<U : Hashable>(_ key: (Iterator.Element) -> U) -> [U:[Iterator.Element]] {
      var dict: [U:[Iterator.Element]] = [:]
      for el in self {
         let key = key(el)
         if case nil = dict[key]?.append(el) { dict[key] = [el] }
      }
      return dict
   }
}
