//
//  AdjustableTableHeight.swift
//  LAFD
//
//  Created by Anthony Picciano on 11/22/16.
//  Copyright © 2016 TALLYGO. All rights reserved.
//

import Foundation
import UIKit

protocol AdjustableTableHeight {
    
    weak var tableView: UITableView! { get set }
    func adjustTableHeight()
    
}

extension AdjustableTableHeight where Self: UITableViewDataSource {
 
    func adjustTableHeight() {
        let rows = tableView(tableView, numberOfRowsInSection: 0)
        let rowHeight = tableView.rowHeight > 0 ? tableView.rowHeight : 44
        
        
        tableView.frame.size.height = CGFloat(rows)
            * rowHeight
            + tableView.contentInset.top
    }
    
}
